﻿-- (18) Realizar una consulta en la que se muestre por cada código hospital el nombre de las especialidades que tiene. Ordenar por código de hospital y especialidad

SELECT DISTINCT cod_hospital,especialidad FROM medicos
ORDER BY 1,2;



SELECT * FROM medicos ;

-- (4) Visualizar el nombre de los empleados vendedores del departamento VENTAS (Nombre del departamento=VENTAS, oficio=VENDEDOR).

  SELECT apellido FROM depart JOIN emple USING(dept_no)
  WHERE dnombre = 'VENTAS' AND oficio = 'VENDEDOR';

-- (6) Visualizar los oficios de los empleados del departamento VENTAS.

SELECT dept_no FROM depart 
  WHERE dnombre = 'VENTAS';

SELECT DISTINCT oficio FROM emple 
  WHERE dept_no = (
    SELECT dept_no FROM depart 
    WHERE dnombre = 'VENTAS'
  );

-- (5) Visualizar el número de vendedores del departamento VENTAS (utilizar la función COUNT sobre la consulta (4)).

  SELECT COUNT(*) FROM emple 
  WHERE dept_no = (
    SELECT dept_no FROM depart 
    WHERE dnombre = 'VENTAS'
  ) AND oficio = 'VENDEDOR';

 -- (1) Visualizar el número de empleados de cada departamento. Utilizar GROUP BY para agrupar por departamento.

SELECT dept_no, COUNT(*) FROM emple 
  GROUP BY dept_no;

-- (3) Hallar la media de los salarios de cada código de departamento con empleados (utilizar la función AVG y GROUP BY).

SELECT dept_no,AVG(salario) FROM emple 
  GROUP BY dept_no;