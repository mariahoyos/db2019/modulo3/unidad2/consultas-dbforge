﻿-- (14) Mostrar los departamentos que tengan más de dos personas trabajando en la misma profesión.

SELECT dept_no FROM emple
  GROUP BY dept_no, oficio HAVING COUNT(*)>2;

-- (11) Visualizar la suma de salarios de cada oficio del departamento VENTAS.

SELECT dept_no FROM depart 
  WHERE dnombre = 'VENTAS';

SELECT oficio,SUM(salario) FROM emple 
  WHERE dept_no = (SELECT dept_no FROM depart 
  WHERE dnombre = 'VENTAS')
  GROUP BY oficio;

-- (9) Mostrar los departamentos cuya suma de salarios sea mayor que la media de salarios de todos los empleados.

SELECT AVG(salario) FROM emple;

  SELECT dept_no FROM emple 
    GROUP BY dept_no HAVING SUM(salario)>(SELECT AVG(salario) FROM emple);

-- (12) Visualizar el número de departamento que tenga más empleados cuyo oficio sea empleado.

SELECT dept_no, COUNT(*)n FROM emple 
  WHERE oficio = 'EMPLEADO'
  GROUP BY dept_no;

SELECT MAX(n) FROM (
  SELECT dept_no, COUNT(*)n FROM emple 
  WHERE oficio = 'EMPLEADO'
  GROUP BY dept_no
  )c1;

SELECT c1.dept_no FROM (
  SELECT dept_no, COUNT(*)n FROM emple 
  WHERE oficio = 'EMPLEADO'
  GROUP BY dept_no
  )c1 WHERE n = (SELECT MAX(n) FROM (
  SELECT dept_no, COUNT(*)n FROM emple 
  WHERE oficio = 'EMPLEADO'
  GROUP BY dept_no
  )c1);

-- (8) Visualizar el departamento con más empleados.

SELECT dept_no, COUNT(*)n FROM emple
  GROUP BY dept_no;

SELECT MAX(n) FROM (
  SELECT dept_no, COUNT(*)n FROM emple
  GROUP BY dept_no
  )c1;

SELECT dept_no FROM (
  SELECT dept_no, COUNT(*)n FROM emple
  GROUP BY dept_no
  )c1
  WHERE n=(SELECT MAX(n) FROM (
  SELECT dept_no, COUNT(*)n FROM emple
  GROUP BY dept_no
  )c1);

-- (29) Mostrar el número de empleados de cada departamento. En la salida se debe mostrar también los departamentos que no tienen ningún empleado.

SELECT dept_no, COUNT(*)n FROM emple
  GROUP BY dept_no;

SELECT DISTINCT dnombre,IFNULL(n,0) n FROM (
  SELECT dept_no, COUNT(*)n FROM emple
  GROUP BY dept_no
  )c1 right JOIN depart ON c1.dept_no = depart.dept_no;