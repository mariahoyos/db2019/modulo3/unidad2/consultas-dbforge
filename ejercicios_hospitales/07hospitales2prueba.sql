﻿-- (10) Para cada oficio obtener la suma de salarios.

  SELECT oficio, SUM(salario) FROM emple 
    GROUP BY oficio;

-- (15) Dada la tabla HERRAMIENTAS, visualizar por cada estantería la suma de las unidades.

SELECT estanteria,SUM(unidades) FROM herramientas 
  GROUP BY estanteria;

-- (19) Realizar una consulta en la que aparezca por cada hospital y en cada especialidad el número de médicos (tendrás que partir de la consulta anterior y utilizar GROUP BY).

SELECT cod_hospital,especialidad,COUNT(*) FROM medicos 
  GROUP BY cod_hospital, especialidad;

-- (13) Mostrar el número de oficios distintos de cada departamento.

  SELECT dept_no, COUNT(DISTINCT oficio) FROM emple 
    GROUP BY dept_no;

-- A partir de la tabla EMPLE, visualizar el número de empleados de cada departamento cuyo oficio sea EMPLEADO (utilizar GROUP BY para agrupar por departamento. En la cláusula WHERE habrá que indicar que el oficio es EMPLEADO).

SELECT dept_no, COUNT(*) FROM emple 
  WHERE oficio = 'EMPLEADO'
  GROUP BY dept_no;

-- (2) Visualizar los departamentos con más de 5 empleados. Utilizar GROUP BY para agrupar por departamento y HAVING para establecer la condición sobre los grupos.

SELECT dept_no FROM emple 
  GROUP BY dept_no HAVING COUNT(*)>5;