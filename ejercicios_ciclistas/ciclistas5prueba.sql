﻿-- (07c) ¿Qué ciclistas han ganado etapas llevando un maillot?

SELECT DISTINCT nombre FROM ciclista JOIN etapa USING (dorsal) JOIN lleva USING(dorsal,numetapa);

-- (07b) Qué códigos de maillots ha llevado Alfonso Gutiérrez en los puertos que haya ganado

SELECT dorsal FROM ciclista
  WHERE nombre = 'Alfonso Gutiérrez';

SELECT numetapa FROM puerto
  WHERE dorsal = (
    SELECT dorsal FROM ciclista
      WHERE nombre = 'Alfonso Gutiérrez'
  );

SELECT código FROM lleva
  WHERE dorsal = (
    SELECT dorsal FROM ciclista
    WHERE nombre = 'Alfonso Gutiérrez'
  ) AND numetapa IN(
    SELECT numetapa FROM puerto
  WHERE dorsal = (
    SELECT dorsal FROM ciclista
      WHERE nombre = 'Alfonso Gutiérrez'
  )
  );

-- (07d) ¿Cuántos ciclistas han ganado etapas llevando un maillot?

SELECT DISTINCT nombre FROM ciclista JOIN etapa USING (dorsal) JOIN lleva USING(dorsal, numetapa);

SELECT COUNT(*) FROM (
SELECT DISTINCT nombre FROM ciclista JOIN etapa USING (dorsal) JOIN lleva USING(dorsal, numetapa)
)c1;

-- (02b) Cuántos puertos ha ganado Alfonso Gutiérrez llevando algún maillot

SELECT dorsal FROM ciclista
  WHERE nombre = 'Alfonso Gutiérrez';

SELECT COUNT(*) FROM  (
  SELECT dorsal FROM ciclista
  WHERE nombre = 'Alfonso Gutiérrez'
  )c1 JOIN puerto USING (dorsal) JOIN lleva USING(dorsal,numetapa);

-- (16) ¿Qué maillots han sido llevados por más de un equipo? Mostrar su código, no es necesario el color

SELECT DISTINCT dorsal, código FROM lleva;

SELECT nomequipo, código FROM (
  SELECT DISTINCT dorsal, código FROM lleva
  )c1 JOIN ciclista USING(dorsal);

SELECT código FROM (
    SELECT nomequipo, código FROM (
      SELECT DISTINCT dorsal, código FROM lleva
    )c1 JOIN ciclista USING(dorsal)
  )c2 GROUP BY código HAVING COUNT(*)>1;

-- otra

SELECT nomequipo,código FROM 
  lleva JOIN ciclista USING(dorsal);

SELECT código FROM (
  SELECT DISTINCT nomequipo,código FROM 
  lleva JOIN ciclista USING(dorsal)
  )c1 GROUP BY código HAVING COUNT(*)>1;

-- (o) Obtener los datos de las etapas cuyos puertos (todos) superan los 1300 metros de altura

SELECT nompuerto FROM puerto
  WHERE altura<=1300;

SELECT DISTINCT numetapa FROM 
  puerto LEFT JOIN (SELECT nompuerto FROM puerto WHERE altura<=1300)c1 ON puerto.nompuerto = c1.nompuerto WHERE c1.nompuerto IS null;

SELECT etapa.* FROM (
  SELECT DISTINCT numetapa FROM 
  puerto LEFT JOIN (SELECT nompuerto FROM puerto WHERE altura<1300)c1 ON puerto.nompuerto = c1.nompuerto WHERE c1.nompuerto IS null
  )c2 JOIN etapa USING(numetapa);

-- otra

SELECT numetapa FROM puerto
  WHERE altura<=1300;

SELECT DISTINCT numetapa FROM puerto;

SELECT c1.numetapa FROM (
    SELECT DISTINCT numetapa FROM puerto
  )c1
  LEFT JOIN
  (
  SELECT numetapa FROM puerto
  WHERE altura<=1300
  )c2 ON c1.numetapa=c2.numetapa WHERE c2.numetapa IS null;

SELECT etapa.* FROM (
    SELECT c1.numetapa FROM (
      SELECT DISTINCT numetapa FROM puerto
    )c1
    LEFT JOIN
    (
    SELECT numetapa FROM puerto
    WHERE altura<=1300
    )c2 ON c1.numetapa=c2.numetapa WHERE c2.numetapa IS null
  )c3 JOIN etapa USING(numetapa);

-- (i) Obtener el código y el color de aquellos maillots que sólo han sido llevados por ciclistas de un mismo equipo

SELECT  DISTINCT código, nomequipo FROM lleva JOIN ciclista USING (dorsal);

SELECT código FROM (
  SELECT DISTINCT código, nomequipo FROM lleva JOIN ciclista USING (dorsal)
  )c1 GROUP BY código HAVING COUNT(*)=1;

SELECT código, color FROM (
  SELECT código FROM (
    SELECT DISTINCT código, nomequipo FROM lleva JOIN ciclista USING (dorsal)
    )c1 GROUP BY código HAVING COUNT(*)=1
)c3 JOIN maillot USING(código);

-- (h) Obtener los datos de los ciclistas que han vestido todos los maillots (no necesariamente en la misma etapa)

SELECT COUNT(*) FROM maillot;

SELECT DISTINCT dorsal,código FROM lleva;

SELECT * FROM maillot;

SELECT dorsal FROM (
  SELECT DISTINCT dorsal,código FROM lleva
  )c2 GROUP BY dorsal HAVING COUNT(*) = (SELECT COUNT(*) FROM maillot);

SELECT ciclista.* FROM (
  SELECT dorsal FROM (
  SELECT DISTINCT dorsal,código FROM lleva
  )c2 GROUP BY dorsal HAVING COUNT(*) = (SELECT COUNT(*) FROM maillot)
  )c3 JOIN ciclista USING(dorsal);

-- (o2) Obtener los datos de las etapas cuyos puertos (todos) superan los 1300 metros de altura

SELECT numetapa FROM puerto
  WHERE altura<=1300;

SELECT DISTINCT numetapa FROM puerto;

SELECT c1.numetapa FROM (
  SELECT DISTINCT numetapa FROM puerto
  )c1 LEFT JOIN (
  SELECT numetapa FROM puerto
  WHERE altura<=1300
  )c2 ON c1.numetapa = c2.numetapa WHERE c2.numetapa IS null;

SELECT etapa.* FROM (
    SELECT c1.numetapa FROM (
    SELECT DISTINCT numetapa FROM puerto
    )c1 LEFT JOIN (
    SELECT numetapa FROM puerto
    WHERE altura<=1300
    )c2 ON c1.numetapa = c2.numetapa WHERE c2.numetapa IS null
 )c3
JOIN etapa USING(numetapa);

-- (05) Nombre de los ciclistas que han ganado todos los puertos de alguna etapa. Ordena por nombre.

SELECT numetapa, COUNT(*)n FROM puerto
  GROUP BY numetapa;

SELECT dorsal,numetapa, COUNT(*)n FROM puerto
  GROUP BY dorsal,numetapa;

SELECT dorsal FROM (
  SELECT numetapa, COUNT(*)n FROM puerto GROUP BY numetapa
  )c1
  JOIN (
  SELECT dorsal,numetapa, COUNT(*)n FROM puerto
  GROUP BY dorsal,numetapa
  )c2 USING (numetapa,n);

SELECT nombre FROM (
    SELECT dorsal FROM (
    SELECT numetapa, COUNT(*)n FROM puerto GROUP BY numetapa
  )c1
  JOIN (
    SELECT dorsal,numetapa, COUNT(*)n FROM puerto
    GROUP BY dorsal,numetapa
  )c2 USING (numetapa,n)
  )c3 JOIN ciclista USING(dorsal) ORDER BY nombre;

-- (z5) Número de la etapa más larga

SELECT MAX(kms) FROM etapa;

SELECT numetapa FROM etapa
  WHERE kms = (SELECT MAX(kms) FROM etapa);

-- (z3) ¿Cuál es la altura y pendiente media de cada categoría de puerto de montaña?

SELECT categoria, AVG(altura), AVG(pendiente) FROM puerto
  GROUP BY categoria;

-- (z4) Nombre de los ciclistas que han ganado puertos de categoría 2

SELECT DISTINCT dorsal FROM puerto
  WHERE categoria=2;

SELECT nombre FROM ciclista JOIN (
  SELECT DISTINCT dorsal FROM puerto
  WHERE categoria=2
  )c1 USING(dorsal);

-- (z2) ¿Cuáles son las etapas con más puertos de categoría especial?

SELECT * FROM puerto;

SELECT nompuerto,numetapa FROM puerto
  WHERE categoria='E';

SELECT numetapa,COUNT(*)n FROM (
  SELECT nompuerto,numetapa FROM puerto
  WHERE categoria='E'
  )c1 GROUP BY numetapa;
  
SELECT MAX(n) FROM (
  SELECT numetapa,COUNT(*)n FROM (
  SELECT nompuerto,numetapa FROM puerto
  WHERE categoria='E'
  )c1 GROUP BY numetapa
  )c2;


SELECT numetapa FROM (
  SELECT nompuerto,numetapa FROM puerto
  WHERE categoria='E'
  )c1 GROUP BY numetapa HAVING COUNT(*) = (
    SELECT MAX(n) FROM (
  SELECT numetapa,COUNT(*)n FROM (
  SELECT nompuerto,numetapa FROM puerto
  WHERE categoria='E'
  )c1 GROUP BY numetapa
  )c2);

-- (z6) Ciudad más utilizada en la vuelta

SELECT DISTINCT salida ciudad FROM etapa;
SELECT DISTINCT llegada ciudad FROM etapa;

SELECT DISTINCT salida ciudad,numetapa FROM etapa
UNION ALL 
SELECT DISTINCT llegada ciudad,numetapa FROM etapa;

SELECT ciudad, COUNT(*) FROM (
    SELECT DISTINCT salida ciudad,numetapa FROM etapa
    UNION ALL 
    SELECT DISTINCT llegada ciudad,numetapa FROM etapa
  )c1 GROUP BY ciudad;

SELECT MAX(n) FROM (
  SELECT ciudad, COUNT(*)n FROM (
    SELECT DISTINCT salida ciudad,numetapa FROM etapa
    UNION ALL 
    SELECT DISTINCT llegada ciudad,numetapa FROM etapa
  )c1 GROUP BY ciudad
)c2;

SELECT ciudad FROM (
    SELECT salida ciudad,numetapa FROM etapa
    UNION ALL 
    SELECT llegada ciudad,numetapa FROM etapa
  )c1 GROUP BY ciudad HAVING COUNT(*)=(
  SELECT MAX(n) FROM (
  SELECT ciudad, COUNT(*)n FROM (
    SELECT DISTINCT salida ciudad,numetapa FROM etapa
    UNION ALL 
    SELECT DISTINCT llegada ciudad,numetapa FROM etapa
  )c1 GROUP BY ciudad
)c2
  );
