﻿-- (d) Obtener los datos de las etapas que no comienzan en la misma ciudad en que acaba la etapa anterior

SELECT etapa.* FROM etapa anterior JOIN etapa etapa ON anterior.numetapa = etapa.numetapa-1
  WHERE etapa.salida<>anterior.llegada;

-- (b) Obtener las poblaciones que tienen la meta de alguna etapa, pero desde las que no se realiza ninguna salida. Resuélvela utilizando NOT IN

SELECT DISTINCT llegada FROM etapa
  WHERE llegada NOT IN (salida);

SELECT DISTINCT salida FROM etapa;

SELECT DISTINCT llegada FROM etapa 
  WHERE llegada NOT IN (SELECT DISTINCT salida FROM etapa);

-- (l) Obtener el nombre de los puertos de montaña que tienen una altura superior a la altura media de todos los puertos

SELECT AVG(altura) FROM puerto;

SELECT nompuerto FROM puerto
  WHERE altura > (SELECT AVG(altura) FROM puerto);

-- (08) ¿Cuántos maillots diferentes ha llevado Miguel Induráin?

SELECT dorsal FROM ciclista
  WHERE nombre='Miguel Induráin';

SELECT COUNT(DISTINCT código) FROM lleva
  WHERE dorsal = (SELECT dorsal FROM ciclista
  WHERE nombre='Miguel Induráin');

-- (a) Obtener los datos de las etapas que pasan por algún puerto de montaña y que tienen salida y llegada en la misma población

SELECT DISTINCT numetapa FROM puerto;

SELECT * FROM etapa
  WHERE numetapa IN (SELECT DISTINCT numetapa FROM puerto) AND salida = llegada;

-- (e) Obtener el número de las etapas que tienen algún puerto de montaña, indicando cuántos tiene cada una de ellas

SELECT numetapa, COUNT(*) FROM puerto
  GROUP BY numetapa;

-- (g) Obtener el nombre y el equipo de los ciclistas que han llevado algún maillot o que han ganado algún puerto

SELECT dorsal FROM lleva
  UNION
SELECT dorsal FROM puerto;

SELECT nombre,nomequipo FROM (
    SELECT DISTINCT dorsal FROM lleva
    UNION
    SELECT DISTINCT dorsal FROM puerto
  )c1 JOIN ciclista USING(dorsal);

-- (c) Obtener el nombre y el equipo de los ciclistas que han ganado alguna etapa llevando el maillot amarillo, mostrando también el número de etapa

SELECT código FROM maillot
  WHERE color='amarillo';

SELECT DISTINCT dorsal,numetapa FROM lleva
  WHERE código = (
    SELECT código FROM maillot
    WHERE color='amarillo'
  );

SELECT nombre, nomequipo, c1.numetapa FROM (
  SELECT DISTINCT lleva.dorsal,numetapa FROM lleva
  WHERE código = (
    SELECT código FROM maillot
    WHERE color='amarillo'
  )
  )c1 JOIN etapa ON c1.dorsal = etapa.dorsal AND c1.numetapa = etapa.numetapa JOIN ciclista ON etapa.dorsal = ciclista.dorsal;

-- (k) Obtener la edad media de los ciclistas que han ganado alguna etapa

SELECT DISTINCT dorsal FROM etapa;

SELECT AVG(edad) FROM (
  SELECT DISTINCT dorsal FROM etapa
  )c1 JOIN ciclista USING(dorsal);

SELECT * FROM ciclista WHERE nombre='miguel induráin';

-- (03) Cuántos puertos ha ganado Induráin llevando algún maillot

SELECT dorsal FROM ciclista
  WHERE nombre = 'Miguel Induráin';

SELECT COUNT(*) FROM (
  SELECT dorsal FROM ciclista
  WHERE nombre = 'Miguel Induráin'
  ) c1 JOIN puerto USING (dorsal) JOIN lleva USING (dorsal,numetapa);

-- (z11) ¿Cuánto dinero se ha repartido en premios?

USE ciclistas;

SELECT numetapa,lleva.código,premio FROM lleva JOIN maillot USING(código);

SELECT SUM(premio) FROM (
  SELECT numetapa,lleva.código,premio FROM lleva JOIN maillot USING(código)
  )c1;



