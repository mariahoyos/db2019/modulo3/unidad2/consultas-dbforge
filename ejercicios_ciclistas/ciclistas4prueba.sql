﻿-- (02) Cuántas etapas con puerto ha ganado Induráin llevando algún maillot

SELECT COUNT(*) FROM (
  SELECT numetapa, dorsal FROM puerto WHERE dorsal = (
   select dorsal from ciclista where nombre ='Miguel Induráin'
  )
)C2 JOIN lleva USING (numetapa, dorsal);

-- (07) Qué maillots ha llevado Induráin en etapas con puerto cuya etapa haya ganado

SELECT dorsal FROM ciclista
  WHERE nombre = 'Miguel Induráin';

SELECT DISTINCT numetapa FROM puerto;

SELECT código FROM (SELECT dorsal FROM ciclista
  WHERE nombre = 'Miguel Induráin')c1 JOIN lleva USING(dorsal) JOIN (SELECT DISTINCT puerto.numetapa FROM puerto)c2 JOIN etapa ON c2.numetapa = etapa.numetapa AND lleva.dorsal = etapa.dorsal;

-- (m) Obtener las poblaciones de salida y de llegada de las etapas donde se encuentran los puertos con mayor pendiente

SELECT MAX(pendiente) FROM puerto;

SELECT numetapa FROM puerto
  WHERE pendiente=( SELECT MAX(pendiente) FROM puerto);

SELECT salida,llegada FROM (
  SELECT numetapa FROM puerto
  WHERE pendiente=( SELECT MAX(pendiente) FROM puerto)
  )c1 JOIN etapa USING(numetapa);

-- (n) Obtener el dorsal y el nombre de los ciclistas que han ganado los puertos de mayor altura

  SELECT MAX(altura) FROM puerto;

  SELECT dorsal FROM puerto
    WHERE altura = (
    SELECT MAX(altura) FROM puerto
    );

  SELECT dorsal, nombre FROM (
    SELECT dorsal FROM puerto
    WHERE altura = (
    SELECT MAX(altura) FROM puerto
    )
    )c1 JOIN ciclista USING(dorsal);

  -- (s) ¿Qué equipos no han ganado ningún puerto de montaña?

SELECT DISTINCT nomequipo FROM ciclista JOIN puerto USING(dorsal);

SELECT equipo.nomequipo FROM equipo LEFT JOIN (SELECT DISTINCT nomequipo FROM ciclista JOIN puerto USING(dorsal))c1 ON c1.nomequipo = equipo.nomequipo WHERE c1.nomequipo IS null;

-- (j) Obtener los números de las etapas que no tienen puertos de montaña

SELECT DISTINCT numetapa FROM puerto;

SELECT etapa.numetapa FROM etapa LEFT JOIN (SELECT DISTINCT numetapa FROM puerto)c1 ON etapa.numetapa = c1.numetapa WHERE c1.numetapa IS NULL;

-- (t) ¿Qué equipos no han ganado etapas con puerto de montaña?

SELECT DISTINCT numetapa FROM puerto;

SELECT dorsal FROM etapa 
  WHERE numetapa IN (SELECT DISTINCT numetapa FROM puerto);

SELECT nomequipo FROM ciclista
  WHERE dorsal IN (
    SELECT dorsal FROM etapa 
    WHERE numetapa IN (SELECT DISTINCT numetapa FROM puerto)
  );

SELECT equipo.nomequipo FROM equipo LEFT JOIN (SELECT nomequipo FROM ciclista
  WHERE dorsal IN (
    SELECT dorsal FROM etapa 
    WHERE numetapa IN (SELECT DISTINCT numetapa FROM puerto)
  ) )c1 ON equipo.nomequipo = c1.nomequipo WHERE c1.nomequipo IS null;

-- (b2) Obtener las poblaciones que tienen la meta de alguna etapa, pero desde las que no se realiza ninguna salida. Resuélvela con un producto externo

SELECT DISTINCT llegada FROM etapa;

SELECT DISTINCT salida FROM etapa;

SELECT llegada FROM (
  SELECT DISTINCT llegada FROM etapa
  )c1 LEFT JOIN (
  SELECT DISTINCT salida FROM etapa
  )c2 ON c1.llegada = c2.salida WHERE c2.salida IS null;

-- (01) En cuántas etapas ha llevado maillot Induráin con puerto y además las ha ganado

SELECT dorsal FROM ciclista
  WHERE nombre = 'Miguel Induráin';

SELECT COUNT(*) FROM (
    SELECT dorsal FROM ciclista
    WHERE nombre = 'Miguel Induráin'
  )c1 JOIN puerto USING(dorsal) JOIN etapa USING(dorsal,numetapa) JOIN lleva USING(dorsal,numetapa);

-- (09) Cuántas etapas con puerto ha ganado Induráin llevando algún maillot

SELECT COUNT(*) FROM (SELECT dorsal FROM ciclista
  WHERE nombre = 'Miguel Induráin')c1 JOIN lleva USING(dorsal) JOIN etapa USING(numetapa,dorsal) JOIN puerto USING(numetapa);

-- (f) Obtener el nombre y la edad de los ciclistas que han llevado dos o más maillots en una misma etapa

SELECT dorsal FROM lleva
  GROUP BY numetapa,dorsal HAVING COUNT(*)>1;

SELECT nombre, edad FROM (
  SELECT dorsal FROM lleva
  GROUP BY numetapa,dorsal HAVING COUNT(*)>1
  )c1 JOIN ciclista USING(dorsal);



SELECT * FROM lleva;

SELECT COUNT(numetapa) FROM etapa;

-- (z7) ¿Cuántos kms hay que recorrer para llegar a la etapa con el puerto más alto?

SELECT MAX(altura) FROM puerto;

SELECT nompuerto, numetapa FROM puerto
  WHERE altura = (SELECT MAX(altura) FROM puerto);

SELECT MIN(numetapa) FROM (SELECT nompuerto, numetapa FROM puerto
  WHERE altura = (SELECT MAX(altura) FROM puerto))c22;

SELECT SUM(kms) FROM etapa 
  WHERE numetapa<(
    SELECT MIN(numetapa) FROM (SELECT nompuerto, numetapa FROM puerto
      WHERE altura = (SELECT MAX(altura) FROM puerto))c22);

-- (z8) ¿Cuántos kms hay que recorrer para llegar a la última etapa con el puerto más alto?

SELECT MAX(altura) FROM puerto;

SELECT numetapa FROM puerto
  WHERE altura = (SELECT MAX(altura) FROM puerto);

SELECT MAX(numetapa) FROM (SELECT numetapa FROM puerto
  WHERE altura = (SELECT MAX(altura) FROM puerto))c22;

SELECT SUM(kms) FROM etapa 
  WHERE numetapa<(
    SELECT MAX(numetapa) FROM (SELECT numetapa FROM puerto
      WHERE altura = (SELECT MAX(altura) FROM puerto))c22);