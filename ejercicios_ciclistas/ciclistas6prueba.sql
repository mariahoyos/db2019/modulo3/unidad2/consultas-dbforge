﻿-- (10) ¿Cuál es la distancia total recorrida?

  SELECT SUM(kms) FROM etapa;

-- (x) ¿Cuántos puertos ha ganado el líder de la montaña?

  SELECT código FROM maillot
  WHERE tipo='montaña';

  SELECT MAX(numetapa) FROM etapa;

 SELECT dorsal FROM lleva
  WHERE código = (
    SELECT código FROM maillot
    WHERE tipo='montaña'
  ) AND numetapa = (SELECT MAX(numetapa) FROM etapa);

SELECT dorsal, COUNT(*) FROM puerto
  GROUP BY dorsal;

SELECT n FROM (
  SELECT dorsal, COUNT(*)n FROM puerto
  GROUP BY dorsal
  )c4 WHERE dorsal = (
   SELECT dorsal FROM lleva
  WHERE código = (
    SELECT código FROM maillot
    WHERE tipo='montaña'
  ) AND numetapa = (SELECT MAX(numetapa) FROM etapa)
  );

-- (z) Genera un ranking del importe en premios de cada equipo. Muestra importe y equipo, ordenados por importe descendente y nombre del equipo.

SELECT nomequipo,ciclista.dorsal,numetapa,lleva.código,premio FROM 
ciclista JOIN lleva ON ciclista.dorsal = lleva.dorsal JOIN maillot ON lleva.código = maillot.código;

SELECT SUM(premio),nomequipo FROM (
  SELECT nomequipo,ciclista.dorsal,numetapa,lleva.código,premio FROM 
  ciclista JOIN lleva ON ciclista.dorsal = lleva.dorsal JOIN maillot ON lleva.código = maillot.código
  )c1 GROUP BY nomequipo
ORDER BY 1 DESC, nomequipo;

-- (y) Genera una tabla en el que figure el nombre del ciclista y los premios acumulados, ordenada por premio descencente y, en caso de coincidir, por nombre

SELECT ciclista.nombre,numetapa,lleva.código,premio FROM lleva JOIN maillot ON lleva.código = maillot.código JOIN ciclista ON lleva.dorsal = ciclista.dorsal;

SELECT nombre, SUM(premio) FROM (
  SELECT ciclista.nombre,numetapa,lleva.código,premio FROM lleva JOIN maillot ON lleva.código = maillot.código JOIN ciclista ON lleva.dorsal = ciclista.dorsal
  )c2 GROUP BY nombre ORDER BY 2 DESC,1;

-- (06) Código de los maillots que han sido llevados por más de un equipo

SELECT distinct nomequipo,código FROM ciclista JOIN lleva ON ciclista.dorsal = lleva.dorsal;

SELECT código FROM (
  SELECT distinct nomequipo,código FROM ciclista JOIN lleva ON ciclista.dorsal = lleva.dorsal
  )c1 GROUP BY código HAVING COUNT(*)>1;

-- (11) ¿Cuál es el equipo con más corredores?

SELECT nomequipo, COUNT(*)n FROM ciclista 
  GROUP BY nomequipo;

SELECT MAX(n) FROM (
  SELECT nomequipo, COUNT(*)n FROM ciclista 
  GROUP BY nomequipo
  )c1;

SELECT nomequipo FROM (
  SELECT nomequipo, COUNT(*)n FROM ciclista 
  GROUP BY nomequipo
  )c2 WHERE n = (
  SELECT MAX(n) FROM (
  SELECT nomequipo, COUNT(*)n FROM ciclista 
  GROUP BY nomequipo
  )c1
  );

-- (r) ¿Qué equipo ha ganado más puertos de montaña?

SELECT nomequipo,nompuerto FROM ciclista JOIN puerto ON ciclista.dorsal = puerto.dorsal;

SELECT nomequipo,COUNT(*)n FROM (
  SELECT nomequipo,nompuerto FROM ciclista JOIN puerto ON ciclista.dorsal = puerto.dorsal
  )c1 GROUP BY 1;

SELECT MAX(n) FROM (
  SELECT nomequipo,COUNT(*)n FROM (
  SELECT nomequipo,nompuerto FROM ciclista JOIN puerto ON ciclista.dorsal = puerto.dorsal
  )c1 GROUP BY 1
  )c2;

SELECT nomequipo FROM (
  SELECT nomequipo,COUNT(*)n FROM (
  SELECT nomequipo,nompuerto FROM ciclista JOIN puerto ON ciclista.dorsal = puerto.dorsal
  )c3 GROUP BY 1
  )c4 WHERE n = (SELECT MAX(n) FROM (
  SELECT nomequipo,COUNT(*)n FROM (
  SELECT nomequipo,nompuerto FROM ciclista JOIN puerto ON ciclista.dorsal = puerto.dorsal
  )c1 GROUP BY 1
  )c2);

-- (u) ¿Qué equipo ha llevado más maillots?

-- c1

SELECT DISTINCT nomequipo,código FROM ciclista JOIN lleva ON ciclista.dorsal = lleva.dorsal;

SELECT nomequipo FROM (
  SELECT DISTINCT nomequipo,código FROM ciclista JOIN lleva ON ciclista.dorsal = lleva.dorsal
  )c1 GROUP BY 1 HAVING COUNT(*) = (SELECT MAX(n) FROM (
    SELECT nomequipo, COUNT(*)n FROM (
    SELECT DISTINCT nomequipo,código FROM ciclista JOIN lleva ON ciclista.dorsal = lleva.dorsal
    )c1 GROUP BY 1
  )c2);

SELECT MAX(n) FROM (
    SELECT nomequipo, COUNT(*)n FROM (
    SELECT DISTINCT nomequipo,código FROM ciclista JOIN lleva ON ciclista.dorsal = lleva.dorsal
    )c1 GROUP BY 1
  )c2;

-- (04) Cuántas etapas sin puerto ha ganado Induráin llevando algún maillot

-- c1

SELECT dorsal FROM ciclista WHERE nombre = 'Miguel Induráin';

-- c2: etapas que ha ganado indurain llevando maillt

SELECT distinct etapa.numetapa FROM (SELECT dorsal FROM ciclista WHERE nombre = 'Miguel Induráin')c1 JOIN etapa USING(dorsal) JOIN lleva USING(dorsal,numetapa);

-- etapas con puerto

SELECT DISTINCT numetapa FROM puerto;

-- etapas - etapas con puerto = etapas sin puerto

SELECT etapa.numetapa FROM etapa LEFT JOIN (SELECT DISTINCT numetapa FROM puerto)c1 ON etapa.numetapa = c1.numetapa WHERE c1.numetapa IS null;

-- resta final

SELECT c2.numetapa FROM 
  (
  SELECT distinct etapa.numetapa FROM (SELECT dorsal FROM ciclista WHERE nombre = 'Miguel Induráin')c1 JOIN etapa USING(dorsal) JOIN lleva USING(dorsal,numetapa)
  )c2 
  JOIN 
  (
  SELECT etapa.numetapa FROM etapa LEFT JOIN (SELECT DISTINCT numetapa FROM puerto)c1 ON etapa.numetapa = c1.numetapa WHERE c1.numetapa IS null
  )c4 ON c2.numetapa= c4.numetapa;

SELECT COUNT(*) FROM (
    SELECT c2.numetapa FROM 
    (
    SELECT distinct etapa.numetapa FROM (SELECT dorsal FROM ciclista WHERE nombre = 'Miguel Induráin')c1 JOIN etapa USING(dorsal) JOIN lleva USING(dorsal,numetapa)
    )c2 
    JOIN 
    (
    SELECT etapa.numetapa FROM etapa LEFT JOIN (SELECT DISTINCT numetapa FROM puerto)c1 ON etapa.numetapa = c1.numetapa WHERE c1.numetapa IS null
    )c4 ON c2.numetapa= c4.numetapa
  )final;

-- (q) ¿Qué ciclista ha ganado más puertos de montaña?

-- c1

SELECT dorsal, COUNT(*)n FROM puerto
  GROUP BY dorsal;

-- c2

SELECT MAX(n) FROM (
  SELECT dorsal, COUNT(*)n FROM puerto
  GROUP BY dorsal
  )c1;

-- c3

SELECT dorsal FROM puerto
  GROUP BY dorsal HAVING 
  COUNT(*) = (
    SELECT MAX(n) FROM (
    SELECT dorsal, COUNT(*)n FROM puerto
    GROUP BY dorsal
    )c1
  );

-- sol

SELECT nombre FROM (
  SELECT dorsal FROM puerto
  GROUP BY dorsal HAVING 
  COUNT(*) = (
    SELECT MAX(n) FROM (
    SELECT dorsal, COUNT(*)n FROM puerto
    GROUP BY dorsal
    )c1
  )
)c2 JOIN ciclista USING(dorsal);


-- (v) ¿Qué equipo ha llevado más maillots diferentes? Indicar cuántos

-- c1

SELECT DISTINCT nomequipo, código FROM ciclista JOIN lleva USING(dorsal);

-- c2

SELECT nomequipo, COUNT(*)n FROM (
    SELECT DISTINCT nomequipo, código FROM ciclista JOIN lleva USING(dorsal)
  )c1 GROUP BY nomequipo;

-- c3

SELECT MAX(n) FROM (
  SELECT nomequipo, COUNT(*)n FROM (
    SELECT DISTINCT nomequipo, código FROM ciclista JOIN lleva USING(dorsal)
  )c1 GROUP BY nomequipo
)c2;

-- c4

SELECT nomequipo, COUNT(*)n FROM (
    SELECT DISTINCT nomequipo, código FROM ciclista JOIN lleva USING(dorsal)
  )c1 GROUP BY nomequipo HAVING n = (
    SELECT MAX(n) FROM (
      SELECT nomequipo, COUNT(*)n FROM (
        SELECT DISTINCT nomequipo, código FROM ciclista JOIN lleva USING(dorsal)
      )c1 GROUP BY nomequipo
    )c2
  );

-- (z1) ¿Cuál es el equipo que más dinero ha acumulado en premios?


-- c1

SELECT DISTINCT nomequipo,ciclista.dorsal,numetapa,lleva.código,premio FROM ciclista JOIN lleva ON ciclista.dorsal = lleva.dorsal JOIN maillot ON lleva.código = maillot.código;

-- c2

SELECT nomequipo, SUM(premio)premio FROM (
  SELECT DISTINCT nomequipo,ciclista.dorsal,numetapa,lleva.código,premio FROM ciclista JOIN lleva ON ciclista.dorsal = lleva.dorsal JOIN maillot ON lleva.código = maillot.código
  )c1 GROUP BY nomequipo;

-- c3
SELECT MAX(premio) FROM (
  SELECT nomequipo, SUM(premio)premio FROM (
  SELECT DISTINCT nomequipo,ciclista.dorsal,numetapa,lleva.código,premio FROM ciclista JOIN lleva ON ciclista.dorsal = lleva.dorsal JOIN maillot ON lleva.código = maillot.código
  )c1 GROUP BY nomequipo
)c2;

-- c4

SELECT nomequipo FROM (
  SELECT DISTINCT nomequipo,ciclista.dorsal,numetapa,lleva.código,premio FROM ciclista JOIN lleva USING(dorsal) JOIN maillot USING(código)
  )c1 GROUP BY nomequipo HAVING SUM(premio) = (
      SELECT MAX(premio) FROM (
      SELECT nomequipo, SUM(premio)premio FROM (
      SELECT DISTINCT nomequipo,ciclista.dorsal,numetapa,lleva.código,premio FROM ciclista JOIN lleva ON ciclista.dorsal = lleva.dorsal JOIN maillot ON lleva.código = maillot.código
      )c1 GROUP BY nomequipo
    )c2
  );


-- (p) Obtener el nombre de los ciclistas, ordenados alfabéticamente, que pertenecen a un equipo de más de cinco ciclistas y que han ganado alguna etapa, indicando también cuántas etapas han ganado

-- c1 ciclistas que han ganado etapas

SELECT DISTINCT dorsal FROM etapa;

-- c1 ciclistas en equipos de más de 5 ciclistas

SELECT dorsal FROM ciclista
  GROUP BY nomequipo HAVING COUNT(*)>5;

SELECT * FROM (
  SELECT dorsal FROM ciclista
  GROUP BY nomequipo HAVING COUNT(*)>5
  )c1 WHERE dorsal IN (
  SELECT DISTINCT dorsal FROM etapa
  );

-- cuenta etapas ganadas

SELECT dorsal, COUNT(*)m FROM etapa
  GROUP BY dorsal;

SELECT nombre,victorias FROM (
  SELECT * FROM (
  SELECT dorsal FROM ciclista
  GROUP BY nomequipo HAVING COUNT(*)>5
  )c1 WHERE dorsal IN (
  SELECT DISTINCT dorsal FROM etapa
  )
)c2 JOIN (
SELECT dorsal, COUNT(*)victorias FROM etapa
  GROUP BY dorsal  
)c3 USING(dorsal) JOIN ciclista USING(dorsal) ORDER BY ciclista.nombre;

-- c1

SELECT nomequipo FROM ciclista
  GROUP BY nomequipo HAVING COUNT(*)>5;

-- c2: ciclistas en equipos de más de 5 ciclistas

SELECT distinct dorsal FROM (SELECT nomequipo FROM ciclista
  GROUP BY nomequipo HAVING COUNT(*)>5)c1 JOIN ciclista USING(nomequipo);

-- c3: ciclistas en equipos de más de 5 ciclistas que han ganado etapas y cuántas etapas han ganado

SELECT dorsal, COUNT(*)n FROM (
  SELECT distinct dorsal FROM (SELECT nomequipo FROM ciclista
  GROUP BY nomequipo HAVING COUNT(*)>5)c1 JOIN ciclista USING(nomequipo)
  )c2 JOIN etapa USING(dorsal)
  GROUP BY dorsal;

-- c4

SELECT nombre,n FROM (
  SELECT dorsal, COUNT(*)n FROM (
  SELECT distinct dorsal FROM (SELECT nomequipo FROM ciclista
  GROUP BY nomequipo HAVING COUNT(*)>5)c1 JOIN ciclista USING(nomequipo)
  )c2 JOIN etapa USING(dorsal)
  GROUP BY dorsal
)c3 JOIN ciclista USING(dorsal)
ORDER BY nombre;