﻿-- (02) Indicar el nombre de las ciudades que tengan una población por encima de la población media

-- c1 media de la poblacion

 SELECT AVG(población)m FROM ciudad;

SELECT nombre FROM ciudad
  WHERE población>(
    SELECT AVG(población)m FROM ciudad
  );

-- (03) Indicar el nombre de las ciudades que tengan una población por debajo de la población media

SELECT nombre FROM ciudad
  WHERE población<(
    SELECT AVG(población)m FROM ciudad
  );

-- (04) Indicar el nombre de la ciudad con la población máxima

  -- c1 max de la poblacion

 SELECT MAX(población) FROM ciudad;

SELECT nombre FROM ciudad
  WHERE población = (
     SELECT MAX(población) FROM ciudad
  );


-- (05) Indicar el nombre de la ciudad con la población mínima

  SELECT nombre FROM ciudad
  WHERE población = (
     SELECT MIN(población) FROM ciudad
  );


-- (6) Indicar el número de ciudades que tengan una población por encima de la población media

-- c1 media de la poblacion

 SELECT AVG(población)m FROM ciudad;

SELECT COUNT(*) FROM ciudad
  WHERE población>(
    SELECT AVG(población) FROM ciudad
  );

USE sedes;

-- (07) Indicarme el número de personas (de la tabla personas) que viven en cada ciudad. No se refiere a la población total de la ciudad.

  SELECT ciudad, COUNT(nombre) FROM persona
  GROUP BY ciudad;

 -- (08) Utilizando la tabla trabaja, indicar cuantas personas trabajan en cada una de las compañías
SELECT compañia, COUNT(*) FROM trabaja
GROUP BY compañia;


-- (09) Indicar la compañía que más trabajadores tiene

SELECT compañia, COUNT(*)n FROM trabaja
GROUP BY compañia
;

SELECT MAX(n) FROM (
    SELECT compañia, COUNT(*)n FROM trabaja
    GROUP BY compañia
    )c1;

SELECT compañia FROM (
  SELECT compañia, COUNT(*)n FROM trabaja
  GROUP BY compañia
  )c2 WHERE n = (
    SELECT MAX(n) FROM (
    SELECT compañia, COUNT(*)n FROM trabaja
    GROUP BY compañia
    )c1
);

SELECT compañia FROM (
  SELECT compañia, COUNT(*)n FROM trabaja
  GROUP BY compañia
  )c2 WHERE n = (
    SELECT MAX(n) FROM (
    SELECT compañia, COUNT(*)n FROM trabaja
    GROUP BY compañia
    )c1
);

  SELECT compañia FROM trabaja
  GROUP BY compañia
  HAVING COUNT(*)= (
    SELECT MAX(n) FROM (
      SELECT compañia, COUNT(*)n FROM trabaja
      GROUP BY compañia
    )c1
  );

  -- (10) Indicarme el salario medio de cada una de las compañías

SELECT compañia, AVG(salario) FROM trabaja
GROUP BY compañia;

-- (20) Indicar el nombre de las personas que no trabajan para FAGOR

-- personas que trabajan en fagor (c1)

SELECT persona FROM trabaja
  WHERE compañia = 'FAGOR';

-- personas que estan en personas y que no estan en c1

SELECT persona.nombre FROM persona LEFT JOIN (
  SELECT persona nombre FROM trabaja
  WHERE compañia = 'FAGOR'
  )c1 ON persona.nombre = c1.nombre WHERE C1.nombre IS NULL;

-- (21) Indicar el número de personas que trabajan para INDRA

SELECT * FROM trabaja
  WHERE compañia='indra';

SELECT COUNT(DISTINCT persona) FROM (
  SELECT * FROM trabaja
  WHERE compañia='indra'
)c1;

