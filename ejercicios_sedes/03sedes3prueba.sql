﻿USE sedes;

-- (01) Indicar el número de ciudades que hay en la tabla ciudades

SELECT COUNT(nombre) FROM ciudad;

-- (11) Listar el nombre de las personas y el número de habitantes de la ciudad donde viven

SELECT * FROM persona;

SELECT * FROM ciudad;

SELECT persona.nombre, ciudad.población FROM persona JOIN ciudad ON persona.ciudad = ciudad.nombre;

-- (12) Listar el nombre de las personas, la calle donde vive y la población de la ciudad donde vive

SELECT persona.nombre, persona.calle, ciudad.población FROM persona JOIN ciudad ON persona.ciudad=ciudad.nombre;

-- (13) Listar el nombre de las personas, la ciudad donde vive y la ciudad donde está la compañía para la que trabaja

SELECT persona.nombre, persona.ciudad, compania.ciudad FROM compania JOIN trabaja ON compania.nombre=trabaja.compañia JOIN persona ON trabaja.persona=persona.nombre JOIN ciudad ON persona.ciudad=ciudad.nombre;

-- (15) Listar el nombre de la persona y el nombre de su supervisor

SELECT persona.nombre, supervisa.supervisor FROM persona JOIN supervisa ON persona.nombre=supervisa.persona;

SELECT persona,supervisor FROM supervisa;

-- (16) Listarme el nombre de la persona, el nombre de su supervisor y las ciudades donde residen cada una de ellos. Ordena por el nombre del supervisado

SELECT * FROM supervisa JOIN (
SELECT nombre FROM sedes.persona ) persona USING (persona);

SELECT * FROM supervisa;
SELECT * FROM persona;


-- c1: nombre de supervisor, nombre persona, ciudad del supervisor

SELECT DISTINCT supervisa.supervisor ns, supervisa.persona np, persona.ciudad cs FROM supervisa JOIN persona ON supervisa.supervisor = persona.nombre;

-- c2: nombre de la persona, ciudad de la persona

SELECT DISTINCT supervisa.persona np, persona.ciudad cp FROM supervisa JOIN persona ON supervisa.persona = persona.nombre;

SELECT DISTINCT c2.np,c1.ns,c2.cp,c1.cs FROM (
  SELECT supervisa.supervisor ns, supervisa.persona np, persona.ciudad cs FROM supervisa JOIN persona ON supervisa.supervisor = persona.nombre
  )c1 INNER JOIN (
  SELECT supervisa.persona np, persona.ciudad cp FROM supervisa JOIN persona ON supervisa.persona = persona.nombre
  )c2 ON c1.np=c2.np
ORDER BY c2.np;

USE sedes;

-- c1

SELECT nombre persona, ciudad cp FROM persona;

-- c2

SELECT nombre supervisor, ciudad cs FROM persona;

-- sol

SELECT c1.persona, c2.supervisor, c1.cp, c2.cs FROM (SELECT nombre persona, ciudad cp FROM persona)c1 JOIN supervisa USING (persona) JOIN (SELECT nombre supervisor, ciudad cs FROM persona)c2 USING(supervisor);






-- usar 2 join y alias (o no) ^_^/

-- (17) Indicar el número de ciudades distintas que hay en la tabla compañía

  SELECT COUNT(ciudad) FROM compania;

-- (18) Indicar el número de ciudades distintas que hay en la tabla personas

SELECT COUNT(DISTINCT ciudad) FROM persona;

-- (19) Indicar el nombre de las personas que trabajan para FAGOR

SELECT persona FROM trabaja
  WHERE compañia='FAGOR';

-- (22) Indicar el nombre de las personas que trabajan para FAGOR o para INDRA

SELECT persona FROM trabaja
  WHERE compañia='FAGOR' OR compañia='INDRA';

-- (23) Listar la población donde vive cada persona, su salario, su nombre y la compañía para la que trabaja. Ordenar la salida por nombre de la persona y por salario de forma descendente.

SELECT persona.ciudad, trabaja.salario, trabaja.persona, trabaja.compañia FROM persona JOIN trabaja ON persona.nombre = trabaja.persona
  ORDER BY trabaja.persona, trabaja.salario DESC;

