﻿-- (41) Autonomías uniprovinciales
SELECT autonomia FROM (
    SELECT autonomia, COUNT(*) n  FROM provincias 
      GROUP BY autonomia  
) c1 WHERE n=1;

SELECT autonomia FROM provincias
      GROUP BY autonomia HAVING COUNT(*)=1;

-- (42) ¿Qué autonomía tiene 5 provincias?
SELECT autonomia FROM (
  SELECT autonomia, COUNT(*) n FROM provincias
  GROUP BY autonomia) c1 WHERE n=5;
  
  SELECT autonomia, COUNT(*) n FROM provincias
  GROUP BY autonomia;

  SELECT autonomia FROM provincias
    GROUP BY autonomia HAVING COUNT(*)=5;
  
-- (43) Población de la autonomía más poblada
  SELECT MAX(n) FROM (SELECT SUM(poblacion) n FROM provincias
    GROUP BY autonomia) c1;
  
  SELECT SUM(poblacion) n FROM provincias
    GROUP BY autonomia;

  SELECT MAX(poblacion) FROM provincias;

-- (44) ¿Qué porcentaje del total nacional representa Cantabria en población y en superficie?

SELECT SUM(poblacion) FROM provincias;

SELECT poblacion FROM provincias
  WHERE (autonomia = 'cantabria');

SELECT (
  
 SELECT poblacion FROM provincias
    WHERE autonomia = 'cantabria'
 )/(
    
  SELECT SUM(poblacion) FROM provincias
  
 )*100
,
  (SELECT superficie FROM provincias
    WHERE autonomia='cantabria'
  )/(
    SELECT SUM(superficie) FROM provincias
  )*100;

-- (45) Automía más extensa
SELECT superficie FROM c1
  WHERE superficie = 
  (SELECT MAX(s) FROM(
    SELECT autonomia, SUM(superficie) s FROM provincias
    GROUP BY autonomia) c1
  ) c2

-- c1
SELECT autonomia, SUM(superficie) s 
FROM provincias
    GROUP BY autonomia;

-- m
SELECT MAX(s) FROM(
    SELECT autonomia, SUM(superficie) s 
    FROM provincias
    GROUP BY autonomia
 ) c1;

--resultado

SELECT autonomia FROM provincias
  GROUP BY autonomia HAVING SUM(superficie) = (
    SELECT MAX(s) FROM(
      SELECT autonomia, SUM(superficie) s 
      FROM provincias
      GROUP BY autonomia
    )c2
  );

SELECT autonomia FROM (
 SELECT autonomia, SUM(superficie) sup FROM provincias
  GROUP BY autonomia 
)c1 WHERE sup = (
    SELECT MAX(sup) FROM(
      SELECT autonomia, SUM(superficie) sup 
      FROM provincias
      GROUP BY autonomia
    )c1
  );

-- (46) Autonomía con más provincias. Utiliza como alias n y c1.

SELECT autonomia FROM(
    SELECT autonomia, COUNT(provincia) n FROM provincias
    GROUP BY autonomia
    ) c1 WHERE n=(
        SELECT MAX(n) FROM(
          SELECT autonomia, COUNT(provincia) n
          FROM provincias
          GROUP BY autonomia
          )c1
        );
    
  


SELECT autonomia, COUNT(provincia) n FROM provincias
    GROUP BY autonomia;

 SELECT MAX(
    SELECT autonomia, COUNT(provincia) n FROM provincias
    GROUP BY autonomia
    ) c1;

-- (47) ¿En qué posición del ranking autonómico por población de mayor a menor está Cantabria?

SELECT poblacion FROM provincias
  WHERE autonomia='Cantabria';

SELECT autonomia, SUM(poblacion) s FROM provincias
  GROUP BY autonomia;



SELECT COUNT(*) FROM(
  SELECT autonomia, SUM(poblacion) pob FROM provincias
  GROUP BY autonomia
)c2 WHERE pob > (
    SELECT poblacion FROM provincias
  WHERE autonomia='Cantabria'
  )c1; 

SELECT autonomia, SUM(poblacion) pob FROM provincias
  GROUP BY autonomia
)c2 WHERE pob > (
    SELECT poblacion FROM provincias
  WHERE autonomia='Cantabria') c1;

SELECT poblacion FROM provincias
  WHERE autonomia='Cantabria';

SELECT autonomia, SUM(poblacion) pob FROM provincias
  GROUP BY autonomia;

-- sacar la tabla the autonomia con poblaciones mayores a la de cantabria
SELECT COUNT(*)+1 FROM(
SELECT autonomia, SUM(poblacion) pob FROM provincias
  GROUP BY autonomia
  HAVING pob > (
    SELECT poblacion FROM provincias
  WHERE autonomia='Cantabria')
)c2;

-- (49) Obtener la provincia más poblada de cada comunidad autónoma, indicando la población de ésta. Mostrar autonomía, provincia y población por orden de aparición en la tabla.

--c1 tabla de autonomias & poblacion total de la autonomia
  SELECT autonomia, SUM(poblacion) s FROM provincias
    GROUP BY autonomia;

--c2 poblacion maxima de autonomias
SELECT MAX(s) FROM(
    SELECT autonomia, SUM(poblacion) s FROM provincias
    GROUP BY autonomia
  )c1;

--c3 la autonomia a la que corresponde la poblacion maxima
SELECT autonomia FROM (
    SELECT autonomia, SUM(poblacion) s FROM provincias
    GROUP BY autonomia
    )c1 WHERE s=
      (SELECT MAX(s) FROM(
      SELECT autonomia, SUM(poblacion) s FROM provincias
      GROUP BY autonomia
      )c2
  );

-- c4 los datos de las provincias de andalucia

SELECT * FROM provincias
  WHERE autonomia=(
    SELECT autonomia FROM (
    SELECT autonomia, SUM(poblacion) s FROM provincias
    GROUP BY autonomia
    )c1 WHERE s=
      (SELECT MAX(s) FROM(
      SELECT autonomia, SUM(poblacion) s FROM provincias
      GROUP BY autonomia
      )c2
      )
  );

-- c5 mínimo de la poblacion de las provincias de andalucia

SELECT MIN(poblacion) FROM (
  SELECT * FROM provincias
  WHERE autonomia=(
    SELECT autonomia FROM (
    SELECT autonomia, SUM(poblacion) s FROM provincias
    GROUP BY autonomia
    )c1 WHERE s=
      (SELECT MAX(s) FROM(
        SELECT autonomia, SUM(poblacion) s FROM provincias
        GROUP BY autonomia
        )c2
      )
    )
  )c3;

-- nombre de la provincia a la que corresponde la poblacion mínima de las provincias de andalucia

  SELECT provincia FROM provincias 
    WHERE poblacion=(
      SELECT MIN(poblacion) FROM (
  SELECT * FROM provincias
  WHERE autonomia=(
    SELECT autonomia FROM (
    SELECT autonomia, SUM(poblacion) s FROM provincias
    GROUP BY autonomia
    )c1 WHERE s=
      (SELECT MAX(s) FROM(
        SELECT autonomia, SUM(poblacion) s FROM provincias
        GROUP BY autonomia
        )c2
      )
    )
  )c3
    ) AND 
    autonomia=(
      SELECT autonomia FROM (
    SELECT autonomia, SUM(poblacion) s FROM provincias
    GROUP BY autonomia
    )c1 WHERE s=
      (SELECT MAX(s) FROM(
      SELECT autonomia, SUM(poblacion) s FROM provincias
      GROUP BY autonomia
      )c2
  )
    );

-- (49) Obtener la provincia más poblada de cada comunidad autónoma, indicando la población de ésta. Mostrar autonomía, provincia y población por orden de aparición en la tabla.

-- c1 población más alta de cada autonomia

  SELECT autonomia, MAX(poblacion) maximo FROM provincias
    GROUP BY autonomia;

-- RESULTADO DE LA CONSULTA USANDO ON

  SELECT provincias.autonomia, provincias.provincia, provincias.poblacion FROM provincias JOIN 
    (SELECT autonomia, MAX(poblacion) maximo FROM provincias
    GROUP BY autonomia) c1 ON provincias.poblacion = maximo AND provincias.autonomia=c1.autonomia;

 -- RESULTADO DE LA CONSULTA USANDO USING
SELECT provincias.autonomia, provincias.provincia, provincias.poblacion FROM provincias JOIN 
    (SELECT autonomia, MAX(poblacion) poblacion FROM provincias
    GROUP BY autonomia) c1 USING (poblacion,autonomia);


-- (50) Provincia más poblada de la autonomía más despoblada

-- c1 población de cada autonomia

  SELECT autonomia, SUM(poblacion) s FROM provincias
  GROUP BY autonomia;

-- m poblacion de la autonomia más despoblada

  SELECT MIN(s) m FROM (
    SELECT autonomia, SUM(poblacion) s FROM provincias
    GROUP BY autonomia 
    )c2;

 -- c2 nombre de la autonomia mas despoblada con HAVING

SELECT * FROM (
    SELECT autonomia, SUM(poblacion) s FROM provincias
      GROUP BY autonomia  
  ) c1 WHERE s=(
      SELECT MIN(s) m FROM (
        SELECT autonomia, SUM(poblacion) s FROM provincias
        GROUP BY autonomia 
        )c2
);


SELECT autonomia FROM(

    SELECT autonomia FROM provincias
    GROUP BY autonomia
    HAVING  SUM(poblacion)=(
      SELECT MIN(habs) FROM (
        SELECT autonomia, SUM(poblacion) habs FROM provincias
        GROUP BY autonomia )c2
        )
  );

-- C2 nombre de la autonomia más despoblada con WHERE

  SELECT autonomia FROM (
      SELECT autonomia, SUM(poblacion) s FROM provincias
      GROUP BY autonomia
      )c1
    WHERE s = (
      
      SELECT MIN(s) m FROM (
      SELECT autonomia, SUM(poblacion) s FROM provincias
      GROUP BY autonomia 
      )c2
  
  );

-- 50 copiada

SELECT provincia FROM provincias
  WHERE poblacion=(
    SELECT MAX(poblacion) FROM provincias
      WHERE autonomia=(
        SELECT autonomia FROM provincias
        GROUP BY autonomia
        HAVING SUM(poblacion) = (
          SELECT MIN(habs) FROM(
            SELECT autonomia, SUM(poblacion) habs FROM
            provincias GROUP BY autonomia
          )c1
      )
  )
  )
  AND autonomia=(
    SELECT autonomia FROM provincias
    GROUP BY autonomia
    HAVING SUM(poblacion) = (
      SELECT MIN(habs) FROM (
        SELECT autonomia, SUM(poblacion) habs FROM provincias
        GROUP BY autonomia)c1
    )
  );

-- (51) Muestra las provincias de Galicia, indicando si es Grande, Mediana o Pequeña en función de si su población supera el umbral de un millón o de medio millón de habitantes.

SELECT OrderID, Quantity,
CASE
    WHEN Quantity > 30 THEN "The quantity is greater than 30"
    WHEN Quantity = 30 THEN "The quantity is 30"
    ELSE "The quantity is under 30"
END AS QuantityText
FROM OrderDetails;

SELECT provincia,
  CASE
    WHEN poblacion > 1e6 THEN "grande"
    WHEN poblacion > 5e5 THEN "mediana"
    ELSE "pequeña"
    END 
  FROM provincias WHERE autonomia='Galicia';

  
  


    
 
    



