﻿-- (10) Autonomías terminadas en 'ana'
  SELECT DISTINCT autonomia FROM provincias 
    WHERE autonomia LIKE '%ana';
-- (11) ¿Cuántos caracteres tiene cada nombre de comunidad autónoma? Ordena el resultado por el nombre de la autonomía de forma descendente
SELECT DISTINCT autonomia, LENGTH(autonomia) FROM provincias 
  ORDER BY autonomia DESC;
-- (12) ¿Qué autonomías tienen nombre compuesto? Ordena el resultado alfabéticamente en orden inverso
SELECT DISTINCT autonomia FROM provincias 
  WHERE autonomia LIKE '% %'
  ORDER BY autonomia DESC;
-- (15) Autonomías que comiencen por 'can' ordenadas alfabéticamente
SELECT DISTINCT autonomia FROM provincias
  WHERE autonomia LIKE 'can%'
  ORDER BY autonomia;
-- (16) ¿Qué autonomías tienen provincias de más de un millón de habitantes? Ordénalas alfabéticamente
  SELECT DISTINCT autonomia FROM provincias
    WHERE poblacion>1000000
    ORDER BY autonomia;
 -- (17) ¿Qué provincias están en autonomías con nombre compuesto?
  SELECT provincia FROM provincias
    WHERE autonomia LIKE '% %';
  -- (19) ¿Qué provincias tienen nombre simple?
SELECT provincia FROM provincias
  WHERE provincia NOT LIKE '% %';

-- (21) Población del país
SELECT SUM(poblacion) FROM provincias;

-- (23) ¿Cuántas provincias hay en la tabla?
SELECT COUNT(*) FROM provincias;

-- (25) ¿Qué comunidades autónomas contienen el nombre de una de sus provincias?
SELECT DISTINCT autonomia FROM provincias
  WHERE LOCATE(provincia,autonomia)<>0;

