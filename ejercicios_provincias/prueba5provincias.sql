﻿SHOW DATABASES;
SHOW TABLES;
-- consulta 1 5ª prueba
SELECT provincia FROM provincias;

-- consulta 2 5ª prueba
SELECT 2+3;

-- consulta 3 5ª prueba
SELECT SQRT(2);

-- (04) Densidades de población de las provincias
SELECT provincia, poblacion / superficie  FROM provincias;

-- (05) ¿Cuántos caracteres tiene cada nombre de provincia?
SELECT provincia, LENGTH(provincia) FROM provincias;

-- (06) Listado de autonomías
SELECT DISTINCT autonomia  FROM provincias; 

-- (07) Provincias con el mismo nombre que su comunidad autónoma
SELECT provincia FROM provincias 
  WHERE provincia=autonomia;

-- (08) Provincias que contienen el diptongo 'ue'
SELECT provincia FROM provincias 
  WHERE provincia LIKE '%ue%';

-- (09) Provincias que empiezan por A
SELECT provincia FROM provincias 
  WHERE provincia LIKE 'a%';

-- (18) ¿Qué provincias tienen nombre compuesto?
  SELECT provincia FROM provincias 
    WHERE provincia LIKE '% %';