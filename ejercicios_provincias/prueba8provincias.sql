﻿-- (31) Provincia más poblada

  SELECT provincia FROM provincias
    WHERE poblacion=(
      SELECT MAX(poblacion) FROM provincias
);

--  (32) Provincia más poblada de las inferiores a 1 millón de habitantes
 SELECT provincia FROM provincias
  WHERE poblacion=(
 SELECT MAX(poblacion) FROM provincias
    WHERE poblacion<1e6
);

-- (33) Listado de autonomías cuyas provincias lleven alguna tilde en su nombre
  SELECT DISTINCT autonomia FROM provincias
    WHERE LOWER(provincia) LIKE '%á%'  COLLATE utf8_bin 
      OR LOWER(provincia) LIKE '%ó%' COLLATE utf8_bin 
      OR LOWER(provincia) LIKE '%é%' COLLATE utf8_bin 
      OR LOWER(provincia) LIKE '%í%' COLLATE utf8_bin
      OR LOWER(provincia) LIKE '%ú%' COLLATE utf8_bin;

 -- (34) Provincia menos poblada de las superiores al millón de habitantes


SELECT MIN(poblacion) FROM provincias
  WHERE poblacion>1e6;
  
SELECT provincia FROM provincias
  WHERE poblacion=(
  SELECT MIN(poblacion) FROM provincias
  WHERE poblacion>1e6);

-- (35) ¿En qué autonomía está la provincia más extensa?

SELECT autonomia FROM provincias
  WHERE superficie=(
  SELECT MAX(superficie) FROM provincias);

-- (36) ¿Qué provincias tienen una población por encima de la media nacional?
SELECT provincia FROM provincias
  WHERE poblacion>(
SELECT AVG(poblacion) FROM provincias);

-- (37) Densidad de población del país

SELECT SUM(poblacion)/SUM(superficie) FROM provincias;
 
-- (38) ¿Cuántas provincias tiene cada comunidad autónoma?

SELECT autonomia, COUNT(provincia) FROM provincias
  GROUP BY autonomia;

SELECT autonomia, COUNT(*)n FROM provincias
  GROUP BY autonomia;

-- (39) Listado del número de provincias por autonomía ordenadas de más a menos provincias y por autonomía en caso de coincidir
SELECT COUNT(*) n, autonomia FROM provincias
  GROUP BY autonomia
ORDER BY n DESC,autonomia;

-- (40) ¿Cuántas provincias con nombre compuesto tiene cada comunidad autónoma?
SELECT autonomia, COUNT(provincia) FROM provincias
  WHERE provincia LIKE '% %'
GROUP BY autonomia;





