﻿-- (13) ¿Qué autonomías tienen nombre simple? Ordena el resultado alfabéticamente en orden inverso
SELECT DISTINCT autonomia FROM provincias
  WHERE autonomia NOT LIKE'% %'
  ORDER BY autonomia DESC;

-- (14) ¿Qué autonomías tienen provincias con nombre compuesto? Ordenar el resultado alfabéticamente
  SELECT DISTINCT autonomia FROM provincias
    WHERE provincia LIKE '% %'
    ORDER BY autonomia;

  -- (20) Listado de provincias y autonomías que contengan la letra ñ
    SELECT  provincia FROM provincias 
      WHERE provincia LIKE '%ñ%' COLLATE utf8_bin
    UNION 
        SELECT  autonomia FROM provincias 
      WHERE autonomia LIKE '%ñ%' COLLATE utf8_bin;

 -- (22) Superficie del país
      SELECT SUM(superficie) FROM provincias;

-- (24) En un listado alfabético, ¿qué provincia estaría la primera?
SELECT MIN(provincia) FROM provincias;

-- (26) ¿Qué provincias tienen un nombre más largo que el de su autonomía?
  SELECT provincia FROM provincias
    WHERE LENGTH(provincia)>LENGTH(autonomia);

-- (27) ¿Cuántas comunidades autónomas hay? (corregir)
-- subConsultas en SQL
SELECT COUNT(*) FROM (
  SELECT DISTINCT autonomia FROM provincias
) c1;

SELECT COUNT(DISTINCT autonomia) FROM provincias;

-- (28) Población media de las provincias entre 2 y 3 millones de habitantes sin decimales

SELECT ROUND( AVG (poblacion)) FROM provincias
  WHERE poblacion>2000000 AND poblacion<3000000;

-- (29) ¿Cuánto mide el nombre de autonomía más corto?

-- SELECT MIN(DISTINCT LENGHT(autonomia)) FROM provincias;

SELECT MIN(LENGTH(autonomia)) FROM provincias;

-- (30) ¿Cuánto mide el nombre de provincia más largo?
SELECT MAX(LENGTH(provincia)) FROM provincias;



