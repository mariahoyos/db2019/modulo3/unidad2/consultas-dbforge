﻿-- (4) Jugador más delgado de la NBA

SELECT MIN(Peso) FROM jugadores;

SELECT Nombre FROM jugadores
  WHERE Peso=(SELECT MIN(Peso) FROM jugadores);

-- (1) Equipos y ciudades de los jugadores españoles de la NBA. No necesito saber el nombre del jugador.

SELECT Nombre_equipo FROM jugadores
 WHERE Procedencia='Spain';

SELECT DISTINCT Nombre, Ciudad FROM (
  SELECT Nombre_equipo Nombre FROM jugadores
    WHERE Procedencia='Spain'
  )c1 JOIN equipos USING(Nombre);

-- (2) Nombre y temporada del jugador con más puntos por partido

SELECT * FROM estadisticas;

SELECT MAX(Puntos_por_partido) FROM estadisticas;

SELECT jugador, temporada FROM estadisticas
  WHERE Puntos_por_partido=(SELECT MAX(Puntos_por_partido) FROM estadisticas);

SELECT Nombre, temporada FROM 
  (
    SELECT jugador, temporada FROM estadisticas
    WHERE Puntos_por_partido=(SELECT MAX(Puntos_por_partido) FROM estadisticas)
  )c1 JOIN jugadores ON c1.jugador = jugadores.codigo;

-- (9) ¿En qué temporada ha anotado más puntos Gasol?

SELECT codigo FROM jugadores
  WHERE Nombre LIKE '%Gasol%';

SELECT MAX(Puntos_por_partido) FROM estadisticas
  WHERE jugador=(SELECT codigo FROM jugadores
  WHERE Nombre LIKE '%Gasol%');

SELECT temporada FROM estadisticas
  WHERE Puntos_por_partido = (
    SELECT MAX(Puntos_por_partido) FROM estadisticas
    WHERE jugador=(SELECT codigo FROM jugadores
    WHERE Nombre LIKE '%Gasol%')
  ) AND jugador=(SELECT codigo FROM jugadores
  WHERE Nombre LIKE '%Gasol%')

