﻿-- (16) Temporada en la que se anotaron más puntos por la totalidad de los equipos

SELECT * FROM estadisticas;

SELECT temporada, SUM(puntos_local)+SUM(puntos_visitante)puntos FROM partidos
  GROUP BY temporada;


SELECT MAX(puntos) FROM (
SELECT temporada, SUM(puntos_local)+SUM(puntos_visitante)puntos FROM partidos
  GROUP BY temporada
)c1;

SELECT temporada FROM partidos
  GROUP BY 1 HAVING SUM(puntos_local)+SUM(puntos_visitante) = 
  (
      SELECT MAX(puntos) FROM (
    SELECT temporada, SUM(puntos_local)+SUM(puntos_visitante)puntos FROM partidos
      GROUP BY 1
    )c1
  );

-- (3) Temporada con estadísticas de más jugadores

SELECT temporada, COUNT(jugador)cuenta FROM estadisticas
  GROUP BY temporada;

SELECT MAX(cuenta) FROM (
  SELECT temporada, COUNT(jugador)cuenta FROM estadisticas
  GROUP BY temporada
  )c1;

SELECT temporada FROM estadisticas
  GROUP BY temporada HAVING COUNT(jugador)=(SELECT MAX(cuenta) FROM (
  SELECT temporada, COUNT(jugador)cuenta FROM estadisticas
  GROUP BY temporada
  )c1);

-- (7) Nombre del equipo que más punto ha anotado en casa

SELECT equipo_local, SUM(puntos_local)s FROM partidos
  GROUP BY equipo_local;

SELECT MAX(s) FROM (
  SELECT equipo_local, SUM(puntos_local)s FROM partidos
  GROUP BY equipo_local
  )c1;

SELECT equipo_local FROM partidos
  GROUP BY equipo_local HAVING SUM(puntos_local)=(
  SELECT MAX(s) FROM (
  SELECT equipo_local, SUM(puntos_local)s FROM partidos
  GROUP BY equipo_local
  )c1
  );

-- (15) ¿Qué equipo ha anotado más puntos?

  SELECT equipo_local equipo, puntos_local puntos FROM partidos
    UNION ALL

    SELECT equipo_visitante equipo, puntos_visitante puntos FROM partidos;

  SELECT equipo, SUM(puntos) FROM (SELECT equipo_local equipo, puntos_local puntos FROM partidos
    UNION ALL

    SELECT equipo_visitante equipo, puntos_visitante puntos FROM partidos)c1
    GROUP BY equipo;

    

    SELECT equipo, SUM(puntos)puntos FROM (
      SELECT equipo_local equipo, puntos_local puntos FROM partidos
      UNION ALL
      SELECT equipo_visitante equipo, puntos_visitante puntos FROM partidos)c1
     GROUP BY equipo;

SELECT MAX(puntos) FROM (
  SELECT equipo, SUM(puntos)puntos FROM (
      SELECT equipo_local equipo, puntos_local puntos FROM partidos
      UNION ALL
      SELECT equipo_visitante equipo, puntos_visitante puntos FROM partidos)c1
     GROUP BY equipo
  )c2;

SELECT equipo FROM (
      SELECT equipo_local equipo, puntos_local puntos FROM partidos
      UNION ALL
      SELECT equipo_visitante equipo, puntos_visitante puntos FROM partidos)c1
     GROUP BY equipo HAVING SUM(puntos)=(SELECT MAX(puntos) FROM (
  SELECT equipo, SUM(puntos)puntos FROM (
      SELECT equipo_local equipo, puntos_local puntos FROM partidos
      UNION ALL
      SELECT equipo_visitante equipo, puntos_visitante puntos FROM partidos)c1
     GROUP BY equipo
  )c2);

-- (8) ¿En qué ciudad hay más equipos de basket?

SELECT Ciudad, COUNT(*)n FROM equipos
  GROUP BY 1;

SELECT MAX(n) FROM (SELECT Ciudad, COUNT(*)n FROM equipos
  GROUP BY 1)c1;

SELECT Ciudad FROM equipos
  GROUP BY 1 HAVING COUNT(*) = (SELECT MAX(n) FROM (SELECT Ciudad, COUNT(*)n FROM equipos
  GROUP BY 1)c1);

-- (13) ¿Qué equipo tiene a los jugadores más ligeros?

SELECT Nombre_equipo, SUM(peso)s FROM jugadores JOIN equipos ON jugadores.Nombre_equipo = equipos.Nombre
  GROUP BY Nombre_equipo;

SELECT MIN(s) FROM (SELECT Nombre_equipo, SUM(peso)s FROM jugadores JOIN equipos ON jugadores.Nombre_equipo = equipos.Nombre
  GROUP BY Nombre_equipo)c1;

SELECT Nombre_equipo FROM jugadores JOIN equipos ON jugadores.Nombre_equipo = equipos.Nombre
  GROUP BY Nombre_equipo HAVING SUM(peso) = (SELECT MIN(s) FROM (SELECT Nombre_equipo, SUM(peso)s FROM jugadores JOIN equipos ON jugadores.Nombre_equipo = equipos.Nombre
  GROUP BY Nombre_equipo)c1);

 -- (18) ¿En qué posición juegan los jugadores más pesados?

  SELECT Nombre_equipo FROM jugadores JOIN equipos ON jugadores.Nombre_equipo = equipos.Nombre
  GROUP BY Nombre_equipo HAVING SUM(peso) = (SELECT MAX(s) FROM (SELECT Nombre_equipo, SUM(peso)s FROM jugadores JOIN equipos ON jugadores.Nombre_equipo = equipos.Nombre
  GROUP BY Nombre_equipo)c1);

  SELECT Nombre_equipo FROM jugadores group by 1
 having round(avg(peso)) = (
select max(p) from(
select Nombre_equipo, ROUND(AVG(peso))p FROM jugadores GROUP BY 1)c1);

-- (14) Equipo que ha ganado más partidos

SELECT * FROM partidos;

-- (17) Nombre de los equipos que anotan en total más puntos jugando fuera que en casa, ordenados alfabéticamente
-- MySQL : Anotar más puntos no implica necesariamente ganar más partidos. Si, en el mismo partido, el otro equipo anota puntualmente más, gana.

SELECT equipo_local equipo, SUM(puntos_local) puntos_casa FROM partidos
  GROUP BY 1;

SELECT equipo_visitante equipo, SUM(puntos_visitante) puntos_fuera FROM partidos
  GROUP BY 1;
  
SELECT equipo FROM 
  (
  SELECT equipo_local equipo, SUM(puntos_local) puntos_casa FROM partidos
  GROUP BY 1
  )c1 JOIN 
  (
  SELECT equipo_visitante equipo, SUM(puntos_visitante) puntos_fuera FROM partidos
  GROUP BY 1
  )c2 USING(equipo)
  WHERE puntos_fuera-puntos_casa>0;

-- (14) Equipo que ha ganado más partidos

SELECT * FROM partidos;

SELECT equipo_local ganador FROM partidos
  WHERE puntos_local>puntos_visitante
UNION all
SELECT equipo_visitante ganador FROM partidos
  WHERE puntos_visitante>puntos_local;

SELECT ganador, COUNT(*)n FROM (
  SELECT equipo_local ganador FROM partidos
  WHERE puntos_local>puntos_visitante
  UNION all
  SELECT equipo_visitante ganador FROM partidos
  WHERE puntos_visitante>puntos_local
  )c1 GROUP BY ganador;

SELECT MAX(n) FROM (
  SELECT ganador, COUNT(*)n FROM (
  SELECT equipo_local ganador FROM partidos
  WHERE puntos_local>puntos_visitante
  UNION all
  SELECT equipo_visitante ganador FROM partidos
  WHERE puntos_visitante>puntos_local
  )c1 GROUP BY ganador
  )c2;

SELECT ganador FROM (
  SELECT equipo_local ganador FROM partidos
  WHERE puntos_local>puntos_visitante
  UNION all
  SELECT equipo_visitante ganador FROM partidos
  WHERE puntos_visitante>puntos_local
  )c1 GROUP BY ganador HAVING COUNT(*) =
  (SELECT MAX(n) FROM (
  SELECT ganador, COUNT(*)n FROM (
  SELECT equipo_local ganador FROM partidos
  WHERE puntos_local>puntos_visitante
  UNION all
  SELECT equipo_visitante ganador FROM partidos
  WHERE puntos_visitante>puntos_local
  )c1 GROUP BY ganador
  )c2);

-- (18) ¿En qué posición juegan los jugadores más pesados?

SELECT posicion, avg(Peso)a FROM jugadores
  GROUP BY Posicion;

SELECT MAX(a) FROM (
  SELECT posicion, ROUND(avg(Peso)a) FROM jugadores
    GROUP BY Posicion
)c1;


SELECT posicion FROM jugadores
  GROUP BY Posicion HAVING avg(Peso) = (
  SELECT MAX(a) FROM (
  SELECT posicion, avg(Peso)a FROM jugadores
    GROUP BY Posicion
)c1
  );

SELECT Posicion FROM jugadores GROUP BY 1
  HAVING ROUND(avg(peso))=(
   SELECT MAX(p) FROM(
    SELECT Posicion,ROUND(avg(peso)) p FROM jugadores GROUP BY 1
  )c1
);