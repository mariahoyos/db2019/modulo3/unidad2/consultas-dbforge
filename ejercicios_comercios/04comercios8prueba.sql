﻿USE comercios;

-- (26b) Obtén un listado de aquellos programas que tengan más de una versión, ordenando la lista por programa y por la versión de más a menos moderna

SELECT nombre FROM programa
  GROUP BY nombre HAVING COUNT(*)>1;

SELECT * FROM programa 
  WHERE nombre IN (SELECT nombre FROM programa
  GROUP BY nombre HAVING COUNT(*)>1)
  ORDER BY nombre, version DESC;

-- (28) ¿Qué comercios distribuyen Windows?

-- c1

SELECT codigo FROM programa WHERE nombre='Windows';

-- sol

SELECT comercio.* FROM (SELECT codigo FROM programa WHERE nombre='Windows')c1 JOIN distribuye USING (codigo) JOIN comercio USING(cif);

SELECT * FROM distribuye;

-- terminar esta con dos subconsultas

-- c1

SELECT codigo FROM programa WHERE nombre='Windows';

-- sol

SELECT comercio.* FROM (
  SELECT codigo FROM programa WHERE nombre='Windows'
  )c1 JOIN distribuye USING (codigo) ;

SELECT comercio.* FROM (
    SELECT * FROM (
      SELECT codigo FROM programa WHERE nombre='Windows'
      )c1 JOIN distribuye USING (codigo)
) c2 JOIN comercio USING (cif);

-- terminada con dos consultas ^_^/

-- (28b) ¿Qué comercios NO distribuyen Windows?

-- c1
SELECT codigo FROM programa
  WHERE nombre = 'Windows';

-- c2

SELECT cif FROM distribuye
  WHERE codigo IN (
    SELECT codigo FROM programa
      WHERE nombre = 'Windows');

-- sol

SELECT * FROM comercio
  WHERE cif NOT IN (SELECT cif FROM distribuye
  WHERE codigo IN (
    SELECT codigo FROM programa
      WHERE nombre = 'Windows'));

-- ---------------------- con join

-- c1

SELECT codigo FROM programa
  WHERE nombre = 'Windows';

-- sol

SELECT comercio.* FROM (
    SELECT codigo FROM programa
    WHERE nombre = 'Windows'
  )c1 JOIN distribuye USING (codigo) RIGHT JOIN comercio USING (cif) WHERE c1.codigo IS NULL;

-- (29) Genera un listado del nombre de los programas y cantidades que se han distribuido a El Corte Inglés de Madrid

-- c1 : cif del corte inglés de madrid

SELECT cif FROM comercio
  WHERE nombre = 'El Corte Inglés' AND ciudad = 'Madrid';

-- codigo y cantidad que ha vendido el corte inglés de madrid

SELECT DISTINCT codigo, cantidad FROM distribuye
  WHERE cif = (
    SELECT cif FROM comercio
     WHERE nombre = 'El Corte Inglés' AND ciudad = 'Madrid');

-- join para sacar el nombre

SELECT programa.*, cantidad FROM programa JOIN (
    SELECT DISTINCT codigo, cantidad FROM distribuye
    WHERE cif = (SELECT cif FROM comercio
    WHERE nombre = 'El Corte Inglés' AND ciudad = 'Madrid')
    )c1 USING (codigo);

-- (37) Obtén un listado de los nombres de las personas que se han registrado por Internet, junto al nombre de los programas para los que ha efectuado el registro

-- c1

SELECT dni,codigo FROM registra
  WHERE medio = 'Internet';

-- sol

SELECT cliente.nombre, programa.nombre FROM programa JOIN (
  SELECT dni,codigo FROM registra
  WHERE medio = 'Internet'
  )c1 USING (codigo) JOIN cliente USING (dni);

SELECT DISTINCT cliente.nombre, programa.nombre FROM (
  SELECT dni,codigo FROM registra
  WHERE medio = 'Internet'
  )c1 JOIN cliente USING (dni) JOIN programa USING(codigo);

-- (39) Genera un listado con las ciudades en las que se pueden obtener los productos de Oracle

-- c1 seleccionar el id de fabricant de oracle

SELECT id_fab FROM fabricante
  WHERE nombre = 'Oracle';

-- joins

SELECT DISTINCT codigo FROM (SELECT id_fab FROM fabricante
  WHERE nombre = 'Oracle')c1 JOIN desarrolla USING (id_fab);

  SELECT DISTINCT cif FROM (SELECT DISTINCT codigo FROM (SELECT id_fab FROM fabricante
  WHERE nombre = 'Oracle')c1 JOIN desarrolla USING (id_fab))c2 JOIN distribuye USING (codigo);

  SELECT ciudad FROM comercio JOIN (SELECT DISTINCT cif FROM (SELECT DISTINCT codigo FROM (SELECT id_fab FROM fabricante
  WHERE nombre = 'Oracle')c1 JOIN desarrolla USING (id_fab))c2 JOIN distribuye USING (codigo))c3 USING (cif);

-- (40) Obtén el nombre de los usuarios que han registrado Access XP

-- codigo

SELECT codigo FROM programa
  WHERE nombre = 'Access' AND version = 'XP';

-- dni

SELECT DISTINCT dni FROM registra JOIN (
  SELECT codigo FROM programa
  WHERE nombre = 'Access' AND version = 'XP'
  )c1 USING (codigo);

SELECT nombre FROM (
  SELECT DISTINCT dni FROM registra JOIN (
    SELECT codigo FROM programa
    WHERE nombre = 'Access' AND version = 'XP'
    )c1 USING (codigo)
)c2 JOIN cliente USING (dni);

-- (53) Obtener el número total de programas que se han distribuido en Sevilla (acabar)

-- cifs de los comercios de sevilla

SELECT cif FROM comercio
  WHERE ciudad = 'Sevilla';

-- codigos de sevilla

  

SELECT SUM(cantidad) FROM distribuye
  WHERE cif IN (
    SELECT cif FROM comercio
    WHERE ciudad = 'Sevilla');

-- con join

SELECT SUM(cantidad) FROM (SELECT cif FROM comercio
  WHERE ciudad = 'Sevilla')c1 JOIN distribuye USING (cif);

-- (54) Calcular el número total de programas que han desarrollado los fabricantes cuyo país es Estados Unidos

-- id_fab de estados unidos

SELECT id_fab FROM fabricante
  WHERE pais='Estados Unidos';

-- codigos de los id_fabs de estados unidos

SELECT COUNT(*) FROM desarrolla JOIN (
  SELECT id_fab FROM fabricante
    WHERE pais='Estados Unidos'
)c1 USING (id_fab);


-- (59) Nombre de los programas que sólo dispongan de una única versión

SELECT * FROM programa;

SELECT nombre FROM programa
  GROUP BY nombre
  HAVING COUNT(*)=1;

SELECT nombre FROM programa
  GROUP BY nombre;

SELECT nombre FROM programa GROUP BY nombre
HAVING COUNT(*)=1;