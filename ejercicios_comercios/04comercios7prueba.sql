﻿-- (27b) Genera un listado de los programas que desarrolla Oracle

-- c1
 SELECT id_fab FROM fabricante
  WHERE nombre='Oracle';

-- c2

SELECT * FROM desarrolla
  WHERE id_fab = (SELECT id_fab FROM fabricante
  WHERE nombre='Oracle');

-- resultado

SELECT * FROM (
  SELECT codigo FROM desarrolla
    WHERE id_fab = (
      SELECT id_fab FROM fabricante
      WHERE nombre='Oracle')
  )c1 JOIN programa USING (codigo);

-- 27-b

-- usando c1

SELECT programa.* FROM desarrolla JOIN (
  SELECT id_fab FROM fabricante
  WHERE nombre='Oracle'
  )c1 USING (id_fab) JOIN programa USING(codigo);

-- (30) ¿Qué fabricante ha desarrollado Freddy Hardest?

-- c1: código del programa Freddy Hardest

SELECT codigo FROM programa
  WHERE nombre = 'Freddy Hardest';

-- resultado utilizando 2 join

SELECT fabricante.nombre FROM (SELECT codigo FROM programa
  WHERE nombre = 'Freddy Hardest')c1 JOIN desarrolla USING(codigo) JOIN fabricante USING (id_fab);

-- resultado usando 2 select

SELECT id_fab FROM desarrolla
  WHERE codigo= (SELECT codigo FROM programa WHERE nombre = 'Freddy Hardest');

SELECT nombre  FROM (
    SELECT id_fab FROM desarrolla
    WHERE codigo= (SELECT codigo FROM programa WHERE nombre = 'Freddy Hardest')
    )c2 JOIN fabricante USING (id_fab);


-- (31) Selecciona el nombre de los programas que se registran por Internet

-- c1 

SELECT codigo FROM registra
  WHERE medio='Internet';

-- resultado

SELECT DISTINCT nombre FROM (SELECT codigo FROM registra
  WHERE medio='Internet')
  c1 JOIN programa USING (codigo);

-- (32) Selecciona el nombre de las personas que se registran por Internet

-- c1

SELECT dni FROM registra
  WHERE medio = 'Internet';

-- resultado

SELECT nombre FROM (
  SELECT DISTINCT dni FROM registra
  WHERE medio = 'Internet'
  )c1 JOIN cliente USING (dni)
ORDER BY nombre;

-- (33) ¿Qué medios ha utilizado para registrarse Pepe Pérez?

-- c1

SELECT dni FROM cliente
  WHERE nombre='Pepe Pérez';

-- sol1

SELECT medio FROM registra 
  WHERE dni = (SELECT dni FROM cliente WHERE nombre='Pepe Pérez');

-- sol2

SELECT DISTINCT medio FROM (
  SELECT dni FROM cliente
  WHERE nombre='Pepe Pérez')
  c1 JOIN registra USING (dni);

-- (35) ¿Qué programas han recibido registros por tarjeta postal?

SELECT DISTINCT codigo FROM registra
  WHERE medio='Tarjeta postal';

-- sol

SELECT DISTINCT nombre FROM (SELECT DISTINCT codigo FROM registra
  WHERE medio='Tarjeta postal')c1 JOIN programa USING (codigo);

-- (36) ¿En qué localidades se han registrado productos por Internet?

SELECT DISTINCT cif FROM registra
  WHERE medio = 'Internet';

-- sol

SELECT DISTINCT ciudad FROM (SELECT DISTINCT cif FROM registra
  WHERE medio = 'Internet')c1 JOIN comercio USING (cif);

-- (43) Genera un listado con los comercios que tienen su sede en la misma ciudad que tiene el comercio FNAC (Subconsulta), excluyendo a la FNAC

SELECT DISTINCT ciudad FROM comercio
  WHERE nombre = 'FNAC';

SELECT DISTINCT nombre FROM (SELECT DISTINCT ciudad FROM comercio
  WHERE nombre = 'FNAC')c1 JOIN comercio USING(ciudad) WHERE nombre<>'FNAC';

SELECT * FROM comercio;

-- (58) Listado de la cantidad de versiones de cada programa ordenados por el nombre del programa

  SELECT nombre, COUNT(*) FROM programa
    GROUP BY nombre
    ORDER BY nombre;