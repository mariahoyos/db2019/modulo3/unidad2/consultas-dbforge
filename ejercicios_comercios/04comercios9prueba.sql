﻿-- (28c) ¿Qué comercios NO distribuyen Windows?

-- c1

SELECT codigo FROM programa
  WHERE nombre = 'Windows';

SELECT DISTINCT cif FROM distribuye
  WHERE codigo IN (
    SELECT codigo FROM programa
    WHERE nombre = 'Windows'
  );

SELECT comercio.* FROM comercio LEFT JOIN (
  SELECT DISTINCT cif FROM distribuye
  WHERE codigo IN (
    SELECT codigo FROM programa
    WHERE nombre = 'Windows'
  )
  )c1 ON c1.cif=comercio.cif WHERE c1.cif IS NULL ;

-- (28d) ¿Qué comercios sólo distribuyen Windows?

  -- c1

  SELECT codigo FROM programa
    WHERE nombre = 'Windows';

  -- c2

    SELECT DISTINCT cif FROM distribuye
      WHERE codigo NOT IN(
        SELECT codigo FROM programa
        WHERE nombre = 'Windows');

  -- c3

    SELECT DISTINCT cif FROM distribuye
      WHERE codigo IN (
        SELECT codigo FROM programa
        WHERE nombre = 'Windows');

  -- c4

    SELECT * FROM (SELECT DISTINCT cif FROM distribuye
      WHERE codigo IN (
        SELECT codigo FROM programa
        WHERE nombre = 'Windows'))c3 LEFT JOIN (SELECT DISTINCT cif FROM distribuye
      WHERE codigo NOT IN(
        SELECT codigo FROM programa
        WHERE nombre = 'Windows'))c2 ON c3.cif=c2.cif WHERE c2.cif IS NULL;
      

-- (65) comercios que no hayan distribuido ningún programa

  SELECT DISTINCT cif FROM distribuye;

  SELECT COUNT(*) FROM comercio
    WHERE cif NOT IN ( SELECT DISTINCT cif FROM distribuye);

  -- (28e) ¿Qué programas sólo se distribuyen en un local? Entendiendo como programas diferentes las distintas versiones de un programa.

    -- c1
    
    SELECT * FROM distribuye
      GROUP BY codigo HAVING COUNT(*)=1;


   -- c2

    SELECT codigo FROM (
       SELECT *, COUNT(*)n FROM distribuye
      GROUP BY codigo HAVING n=1
      )c1;

-- sol

  SELECT nombre,programa.version FROM (
    SELECT codigo FROM (
       SELECT * FROM distribuye
      GROUP BY codigo HAVING COUNT(*)=1
      )c1
    )c2 JOIN programa USING (codigo);

-- (28f) ¿Qué locales comerciales sólo distribuyen un programa?

-- c1 

SELECT DISTINCT cif, nombre FROM distribuye JOIN programa USING(cif);

-- c2

SELECT cif FROM (
  SELECT DISTINCT cif, nombre FROM distribuye JOIN programa USING(codigo)
  )c1 GROUP BY cif HAVING COUNT(*)=1;

-- c3

SELECT * FROM (
  SELECT cif FROM (
  SELECT DISTINCT cif, nombre FROM distribuye JOIN programa USING(codigo)
  )c1 GROUP BY cif HAVING COUNT(*)=1
)c2 JOIN comercio USING(cif);

-- (44) Nombre de aquellos clientes, ordenados alfabéticamente, que han registrado un producto de la misma forma que el cliente Pepe Pérez (Subconsulta)

-- c1 dni de pepe perez
SELECT dni FROM cliente
  WHERE nombre='Pepe Pérez';

-- medio usado por pepe perez

SELECT medio FROM registra
  WHERE dni=( 
  SELECT dni FROM cliente
  WHERE nombre='Pepe Pérez'
  );

-- dnis que han usado el medio c2 sin que sea el dni de pepe perez

SELECT dni FROM registra
  WHERE medio IN (
    SELECT medio FROM registra
    WHERE dni=( 
    SELECT dni FROM cliente
    WHERE nombre='Pepe Pérez'
    )
  ) AND dni<>(
    SELECT dni FROM cliente
    WHERE nombre='Pepe Pérez'
  );

-- nombres de los clientes con dnis de la consulta anterior

SELECT DISTINCT nombre FROM (
  SELECT dni FROM registra
    WHERE medio IN (
      SELECT medio FROM registra
      WHERE dni=( 
      SELECT dni FROM cliente
      WHERE nombre='Pepe Pérez'
      )
    ) AND dni<>(
      SELECT dni FROM cliente
      WHERE nombre='Pepe Pérez'
    )
)c1 JOIN cliente USING (dni) ORDER BY 1;

-- (57) Nombre de los comercios que sólo distribuyen un único programa además de Windows

-- (60) (60) Nombre de los locales que distribuyen únicamente 2 programas. Entendiendo por un único programa a todas sus versiones.

SELECT DISTINCT cif, nombre FROM distribuye JOIN programa USING(codigo);

SELECT cif FROM (
  SELECT DISTINCT cif, nombre FROM distribuye JOIN programa USING(codigo)
  )c1 GROUP BY cif HAVING COUNT(*)=2;

SELECT DISTINCT nombre FROM (SELECT cif FROM (
  SELECT DISTINCT cif, nombre FROM distribuye JOIN programa USING(codigo)
  )c1 GROUP BY cif HAVING COUNT(*)=2)c2 JOIN comercio USING(cif);

-- (61) Nombre de los programas que se distribuyen en un único local, entendiendo como el mismo programa a todas sus versiones

-- c1: nombres y cifs de programas que son distribuidos por comercios

SELECT DISTINCT nombre, cif FROM programa JOIN distribuye USING (codigo);

-- c2: agrupación por nombres de programa y cuenta de registros de la consulta anterior

SELECT nombre FROM (
  SELECT DISTINCT nombre, cif FROM programa JOIN distribuye USING (codigo)
  )c1 GROUP BY nombre HAVING COUNT(*)=1;

-- (62) Nombre de los programas que se distribuyen en un único comercio, entendiendo como el mismo programa a todas sus versiones

-- c1

SELECT DISTINCT programa.nombre np, comercio.nombre nc FROM programa JOIN distribuye USING(codigo) JOIN comercio USING(cif);

-- c2

SELECT np FROM (
    SELECT DISTINCT programa.nombre np, comercio.nombre nc FROM programa JOIN distribuye USING(codigo) JOIN comercio USING(cif)
)c1 GROUP BY np HAVING COUNT(*)=1;

-- (63) Nombres de comercios (no locales comerciales) que tengan exclusivas de distribución. Es decir, que distribuyan programas que sólo ellos distribuyen.

SELECT distinct comercio FROM (
    SELECT DISTINCT programa.nombre programa, comercio.nombre comercio 
      FROM programa JOIN distribuye USING(codigo) 
      JOIN comercio USING(cif)
  )c1 JOIN 
  (
    SELECT programa FROM (
      SELECT DISTINCT programa.nombre programa, comercio.nombre comercio FROM programa JOIN distribuye USING(codigo) JOIN comercio USING(cif)
      )c1 GROUP BY 1 HAVING COUNT(*)=1
  )c2 USING (programa);

-- (64) Ciudades donde se puedan adquirir programas distribuidos en exclusiva por los comercios (no los locales comerciales, aunque sí que hay que tener en cuenta el stock)

SELECT DISTINCT comercio.ciudad  FROM (
    SELECT distinct comercio FROM (
      SELECT DISTINCT programa.nombre programa, comercio.nombre comercio 
        FROM programa JOIN distribuye USING(codigo) 
        JOIN comercio USING(cif)
    )c1 JOIN 
    (
      SELECT programa FROM (
        SELECT DISTINCT programa.nombre programa, comercio.nombre comercio FROM programa JOIN distribuye USING(codigo) JOIN comercio USING(cif)
        )c1 GROUP BY 1 HAVING COUNT(*)=1
    )c2 USING (programa)
  
  )c3 JOIN comercio ON c3.comercio = nombre JOIN distribuye USING(cif);

-- (65) Indica a cuántos comercios no se les distribuye ningún programa

SELECT COUNT(*) FROM comercio LEFT JOIN distribuye ON comercio.cif = distribuye.cif WHERE distribuye.cif IS NULL;

SELECT * FROM desarrolla;