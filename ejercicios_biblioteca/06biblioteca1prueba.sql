﻿-- (i) Obtener los temas de los que no tenemos ninguna obra de Carmen Laforet

SELECT * FROM obra;
SELECT * FROM autor;
SELECT * FROM persona;

SELECT numautor FROM autor
  WHERE nombre = 'Carmen Laforet';

SELECT tema FROM obra
  WHERE numautor = (
    SELECT numautor FROM autor
    WHERE nombre = 'Carmen Laforet');

SELECT tema FROM tema;

SELECT c1.tema FROM (SELECT tema FROM tema)c1 LEFT JOIN (SELECT tema FROM obra
  WHERE numautor = (
    SELECT numautor FROM autor
    WHERE nombre = 'Carmen Laforet'))c2 ON c1.tema = c2.tema WHERE c2.tema IS null;

-- (j) Obtener los datos de los autores de los que sólo tenemos obras de misterio

SELECT * FROM(
SELECT numautor FROM obra WHERE tema = 'misterio')c1
JOIN autor USING(numautor);

-- (g) Para cada autor obtener el número de obras que ha escrito de cada tema distinto

SELECT numautor,tema,COUNT(*)n FROM obra
 GROUP BY numautor,tema;

SELECT * FROM prestamo;

-- (c) Obtener el nombre de las personas que tienen prestados más de tres libros y desde hace más de seis meses

SELECT numpersona FROM prestamo
where fecha<now()-interval 6 month 
group by numpersona having count(*)>3;

-- (i2) Obtener los temas de los que no tenemos ninguna obra de Carmen Laforet

-- c1
  
  SELECT numautor FROM autor
    WHERE nombre = 'Carmen Laforet';

-- c2

  SELECT DISTINCT tema FROM obra
    WHERE numautor = (
    SELECT numautor FROM autor
    WHERE nombre = 'Carmen Laforet'
    );

-- c3

SELECT DISTINCT tema FROM obra;

-- sol

SELECT tema FROM 
  tema
  LEFT JOIN 
  (
  SELECT tema FROM obra
    WHERE numautor = (
    SELECT numautor FROM autor
    WHERE nombre = 'Carmen Laforet'
    )
  )c1
   USING (tema) WHERE c1.tema IS NULL;

SELECT tema FROM tema WHERE tema NOT IN (
  SELECT DISTINCT tema FROM obra
    WHERE numautor = (
    SELECT numautor FROM autor
    WHERE nombre = 'Carmen Laforet'
    )
  );

-- (e) Obtener los datos de las obras que se encuentran en más de un libro, indicando exactamente en cuántos se encuentran

 SELECT * FROM (
select numobra, count(*)n
from esta_en group by numobra having n>1
)c1 join obra using(numobra);

-- (d) Obtener los datos de las personas que en sus préstamos tienen libros de más de tres temas

SELECT DISTINCT tema,numlibro FROM esta_en JOIN obra USING(numobra);

SELECT numlibro FROM (
  SELECT DISTINCT tema,numlibro FROM esta_en JOIN obra USING(numobra)
  )c1 GROUP BY numlibro HAVING COUNT(*)>3;

SELECT persona.* FROM (
  SELECT numlibro FROM (
  SELECT DISTINCT tema,numlibro FROM esta_en JOIN obra USING(numobra)
  )c1 GROUP BY numlibro HAVING COUNT(*)>3
  )c3 JOIN prestamo USING(numlibro) JOIN persona USING(numpersona);


-- (b) Obtener las nacionalidades de las que hay menos autores

SELECT nacionalidad,COUNT(*)n FROM autor
  GROUP BY nacionalidad;

SELECT MIN(n) FROM (SELECT nacionalidad,COUNT(*)n FROM autor
  GROUP BY nacionalidad)c1;

SELECT nacionalidad FROM autor
  GROUP BY nacionalidad HAVING COUNT(*) = (SELECT MIN(n) FROM (SELECT nacionalidad,COUNT(*)n FROM autor
  GROUP BY nacionalidad)c1);

-- (k) Datos de personas que tienen prestados libros de autores con más de una obra

USE biblioteca;

SELECT numlibro, COUNT(*)n FROM esta_en
  GROUP BY numlibro HAVING n>1;

SELECT persona.* FROM (
SELECT numlibro, COUNT(*)n FROM esta_en
  GROUP BY numlibro HAVING n>1
  )c2 JOIN prestamo USING(numlibro) JOIN persona USING(numpersona);


-- (a) Obtener los datos del autor que más obras ha escrito

USE biblioteca;

SELECT numautor,COUNT(*)n FROM obra
  GROUP BY numautor;

SELECT MAX(n) FROM (
  SELECT numautor,COUNT(*)n FROM obra
  GROUP BY numautor
  )c1;

SELECT numautor FROM obra
  GROUP BY numautor HAVING COUNT(*) = (
  SELECT MAX(n) FROM (
  SELECT numautor,COUNT(*)n FROM obra
  GROUP BY numautor
  )c1
  );

SELECT autor.* FROM (
  SELECT numautor FROM obra
  GROUP BY numautor HAVING COUNT(*) = (
  SELECT MAX(n) FROM (
  SELECT numautor,COUNT(*)n FROM obra
  GROUP BY numautor
  )c1
  )
  )c3 JOIN autor USING(numautor);

-- (h) Obtener los números de los libros que contienen obras de más temas diferentes

SELECT DISTINCT tema,numlibro FROM obra JOIN esta_en USING(numobra);

SELECT c1.numlibro, COUNT(*)n FROM (
  SELECT DISTINCT tema,numlibro FROM obra JOIN esta_en USING(numobra)
  )c1 GROUP BY c1.numlibro;

SELECT MAX(n) FROM (
  SELECT c1.numlibro, COUNT(*)n FROM (
  SELECT DISTINCT tema,numlibro FROM obra JOIN esta_en USING(numobra)
  )c1 GROUP BY c1.numlibro
  )c2;

SELECT c1.numlibro FROM (
  SELECT DISTINCT tema,numlibro FROM obra JOIN esta_en USING(numobra)
  )c1 GROUP BY c1.numlibro HAVING COUNT(*) = (SELECT MAX(n) FROM (
  SELECT c1.numlibro, COUNT(*)n FROM (
  SELECT DISTINCT tema,numlibro FROM obra JOIN esta_en USING(numobra)
  )c1 GROUP BY c1.numlibro
  )c2);

-- (a) Obtener los datos del autor que más obras ha escrito

SELECT numautor, COUNT(*)n FROM autor LEFT JOIN obra USING(numautor)
GROUP BY numautor;

SELECT MAX(n) FROM (
  SELECT numautor, COUNT(*)n FROM autor LEFT JOIN obra USING(numautor)
  GROUP BY numautor
  )c1;

SELECT * FROM  (
  SELECT numautor FROM obra GROUP BY numautor
    HAVING COUNT(*) = (
    SELECT MAX(n) FROM (
      SELECT numautor, COUNT(*)n FROM obra
      GROUP BY numautor
      )c1
  ) 
)c3 LEFT JOIN autor USING(numautor);
  