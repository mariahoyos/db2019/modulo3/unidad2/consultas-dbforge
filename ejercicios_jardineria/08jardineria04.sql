﻿-- (4.10.21) Calcula la facturación obtenida con los productos con más margen de beneficio. (Sin excluir los pedidos rechazados)

SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos;

SELECT MAX(beneficio) FROM (
  SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos
  )c1;

SELECT CodigoProducto FROM (
  SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos  
  )c1 WHERE beneficio=(
        SELECT MAX(beneficio) FROM (
          SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos
          )c1
        );


SELECT SUM(PrecioUnidad*Cantidad) FROM 
  (
  SELECT CodigoProducto FROM (
  SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos  
  )c1 WHERE beneficio=(
        SELECT MAX(beneficio) FROM (
          SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos
          )c1
        )
  )c2
  JOIN
  detallepedidos
  USING (CodigoProducto);

-- (01b) Muestra el nombre y el código de cliente de aquellos clientes que han pedido alguno de los productos que nos aporta el mínimo beneficio con su venta. Ordena el resultado por nombre del cliente.

SELECT CodigoProducto FROM (
  SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos  
  )c1 WHERE beneficio=(
        SELECT MIN(beneficio) FROM (
          SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos
          )c1
        );

SELECT DISTINCT NombreCliente, clientes.CodigoCliente FROM (
  SELECT CodigoProducto FROM (
  SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos  
  )c1 WHERE beneficio=(
        SELECT MIN(beneficio) FROM (
          SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos
          )c1
        )
  )c2 JOIN detallepedidos USING(CodigoProducto) JOIN pedidos USING(CodigoPedido) JOIN clientes USING(CodigoCliente)
ORDER BY NombreCliente;

-- (4.08.09) ¿Qué cliente(s) ha rechazado más pedidos?

SELECT CodigoCliente, COUNT(*)n FROM pedidos
  WHERE Estado='Rechazado'
  GROUP BY CodigoCliente;

SELECT MAX(n) FROM (
  SELECT CodigoCliente, COUNT(*)n FROM pedidos
  WHERE Estado='Rechazado'
  GROUP BY CodigoCliente
  )c1;

SELECT CodigoCliente FROM pedidos
  WHERE Estado='Rechazado'
  GROUP BY CodigoCliente HAVING COUNT(*) = (
  SELECT MAX(n) FROM (
    SELECT CodigoCliente, COUNT(*)n FROM pedidos
    WHERE Estado='Rechazado'
    GROUP BY CodigoCliente
    )c1);

SELECT NombreCliente FROM (
  SELECT CodigoCliente FROM pedidos
  WHERE Estado='Rechazado'
  GROUP BY CodigoCliente HAVING COUNT(*) = (
  SELECT MAX(n) FROM (
    SELECT CodigoCliente, COUNT(*)n FROM pedidos
    WHERE Estado='Rechazado'
    GROUP BY CodigoCliente
    )c1)
  )c2 JOIN clientes USING(CodigoCliente);

-- (4.08.05) Obtener la cantidad facturada con el producto que más unidades haya vendido

SELECT CodigoProducto, SUM(Cantidad) FROM detallepedidos
  GROUP BY CodigoProducto;

SELECT MAX(suma) FROM (
  SELECT CodigoProducto, SUM(Cantidad)suma FROM detallepedidos
  GROUP BY CodigoProducto
  )c1;

SELECT CodigoProducto, SUM(Cantidad) FROM detallepedidos
  GROUP BY CodigoProducto HAVING SUM(cantidad) = 
  (
    SELECT MAX(suma) FROM (
      SELECT CodigoProducto, SUM(Cantidad)suma FROM detallepedidos
      GROUP BY CodigoProducto
      )c1
  );

SELECT SUM(Cantidad*PrecioUnidad) FROM 
    (
    SELECT CodigoProducto, SUM(Cantidad) FROM detallepedidos
    GROUP BY CodigoProducto HAVING SUM(cantidad) = 
    (
      SELECT MAX(suma) FROM (
        SELECT CodigoProducto, SUM(Cantidad)suma FROM detallepedidos
        GROUP BY CodigoProducto
        )c1
    )
  )c2 JOIN detallepedidos USING(CodigoProducto);

-- (4.08.06) Obtener la cantidad facturada con el producto que más dinero haya hecho ingresar

SELECT CodigoProducto, Cantidad*PrecioUnidad ingreso FROM detallepedidos;

SELECT CodigoProducto, SUM(ingreso)ingresototal FROM (
  SELECT CodigoProducto, Cantidad*PrecioUnidad ingreso FROM detallepedidos
  )c1 GROUP BY CodigoProducto;

SELECT MAX(ingresototal) FROM 
  (
    SELECT CodigoProducto, SUM(ingreso)ingresototal FROM (
    SELECT CodigoProducto, Cantidad*PrecioUnidad ingreso FROM detallepedidos
    )c1 GROUP BY CodigoProducto
  )c2;

SELECT CodigoProducto FROM (
  SELECT CodigoProducto, Cantidad*PrecioUnidad ingreso FROM detallepedidos
  )c1 GROUP BY CodigoProducto HAVING SUM(ingreso) = 
  (
  SELECT MAX(ingresototal) FROM 
  (
    SELECT CodigoProducto, SUM(ingreso)ingresototal FROM (
    SELECT CodigoProducto, Cantidad*PrecioUnidad ingreso FROM detallepedidos
    )c1 GROUP BY CodigoProducto
  )c2
  );

SELECT SUM(Cantidad*PrecioUnidad) FROM (
  SELECT CodigoProducto FROM (
  SELECT CodigoProducto, Cantidad*PrecioUnidad ingreso FROM detallepedidos
  )c1 GROUP BY CodigoProducto HAVING SUM(ingreso) = 
  (
  SELECT MAX(ingresototal) FROM 
  (
    SELECT CodigoProducto, SUM(ingreso)ingresototal FROM (
    SELECT CodigoProducto, Cantidad*PrecioUnidad ingreso FROM detallepedidos
    )c1 GROUP BY CodigoProducto
  )c2
  )
  )c3 JOIN detallepedidos USING(CodigoProducto);

-- (4.10.29) ¿Cuál es la descripción de la gama del producto que más factura? (Sin tener en cuenta devoluciones)

SELECT CodigoPedido FROM pedidos
  WHERE Estado<>'Rechazado';

SELECT CodigoPedido,CodigoProducto,Cantidad*PrecioUnidad importe FROM detallepedidos;

SELECT CodigoProducto, SUM(importe)importetotal FROM 
  (
  SELECT CodigoPedido,CodigoProducto,Cantidad*PrecioUnidad importe FROM detallepedidos
  )
  c1
  JOIN
  (
  SELECT CodigoPedido FROM pedidos
  WHERE Estado<>'Rechazado'
  )
  c2 USING(CodigoPedido)
GROUP BY CodigoProducto;

SELECT MAX(importetotal) FROM (
  SELECT CodigoProducto, SUM(importe)importetotal FROM 
  (
  SELECT CodigoPedido,CodigoProducto,Cantidad*PrecioUnidad importe FROM detallepedidos
  )
  c1
  JOIN
  (
  SELECT CodigoPedido FROM pedidos
  WHERE Estado<>'Rechazado'
  )
  c2 USING(CodigoPedido)
GROUP BY CodigoProducto
  )c4;

SELECT CodigoProducto FROM 
  (SELECT CodigoPedido,CodigoProducto,Cantidad*PrecioUnidad importe FROM detallepedidos)
  c1
  JOIN (SELECT CodigoPedido FROM pedidos WHERE Estado<>'Rechazado')c2 USING(CodigoPedido)
GROUP BY CodigoProducto HAVING SUM(importe) = (
  SELECT MAX(importetotal) FROM (
  SELECT CodigoProducto, SUM(importe)importetotal FROM 
  (SELECT CodigoPedido,CodigoProducto,Cantidad*PrecioUnidad importe FROM detallepedidos) c1
  JOIN  (
  SELECT CodigoPedido FROM pedidos
  WHERE Estado<>'Rechazado'
  )
  c2 USING(CodigoPedido)
GROUP BY CodigoProducto
  )c4
  );

SELECT DescripcionTexto FROM (
  SELECT CodigoProducto FROM 
  (SELECT CodigoPedido,CodigoProducto,Cantidad*PrecioUnidad importe FROM detallepedidos)
  c1
  JOIN (SELECT CodigoPedido FROM pedidos WHERE Estado<>'Rechazado')c2 USING(CodigoPedido)
GROUP BY CodigoProducto HAVING SUM(importe) = (
  SELECT MAX(importetotal) FROM (
  SELECT CodigoProducto, SUM(importe)importetotal FROM 
  (SELECT CodigoPedido,CodigoProducto,Cantidad*PrecioUnidad importe FROM detallepedidos) c1
  JOIN  (
  SELECT CodigoPedido FROM pedidos
  WHERE Estado<>'Rechazado'
  )
  c2 USING(CodigoPedido)
GROUP BY CodigoProducto
  )c4
  )
  )c6 JOIN productos USING(CodigoProducto) JOIN gamasproductos USING (gama);

-- 30072019

-- c2
SELECT CodigoProducto, SUM(Cantidad*PrecioUnidad) importe FROM detallepedidos
GROUP BY CodigoProducto;

-- c3
SELECT MAX(importe) FROM (
  SELECT CodigoProducto, SUM(Cantidad*PrecioUnidad) importe FROM detallepedidos
  GROUP BY CodigoProducto
  )c2;

-- c4

SELECT CodigoProducto FROM detallepedidos
GROUP BY CodigoProducto HAVING SUM(Cantidad*PrecioUnidad) = 
  (
    SELECT MAX(importe) FROM (
    SELECT CodigoProducto, SUM(Cantidad*PrecioUnidad) importe FROM detallepedidos
    GROUP BY CodigoProducto
    )c2
  );

-- c5

SELECT Gama FROM productos
  WHERE CodigoProducto = (
  SELECT CodigoProducto FROM detallepedidos
GROUP BY CodigoProducto HAVING SUM(Cantidad*PrecioUnidad) = 
  (
    SELECT MAX(importe) FROM (
    SELECT CodigoProducto, SUM(Cantidad*PrecioUnidad) importe FROM detallepedidos
    GROUP BY CodigoProducto
    )c2
  )
  );

-- c6

SELECT DescripcionTexto FROM gamasproductos
  WHERE Gama = (
  SELECT Gama FROM productos
  WHERE CodigoProducto = (
  SELECT CodigoProducto FROM detallepedidos
GROUP BY CodigoProducto HAVING SUM(Cantidad*PrecioUnidad) = 
  (
    SELECT MAX(importe) FROM (
    SELECT CodigoProducto, SUM(Cantidad*PrecioUnidad) importe FROM detallepedidos
    GROUP BY CodigoProducto
    )c2
  )
  )
  );

SELECT DescripcionTexto FROM(
SELECT CodigoProducto
FROM detallepedidos GROUP BY CodigoProducto
HAVING SUM(Cantidad * PrecioUnidad)=(
SELECT MAX(s) FROM(
SELECT CodigoProducto, SUM(Cantidad*PrecioUnidad) s
FROM detallepedidos GROUP BY CodigoProducto
)c1
)
)c2 JOIN productos USING(CodigoProducto) JOIN gamasproductos USING(Gama);

-- (4.10.33) Código de las oficinas que no venden frutales

SELECT * FROM productos;

SELECT DISTINCT CodigoProducto FROM productos
  WHERE Gama='Frutales';

SELECT DISTINCT CodigoOficina FROM (
  SELECT DISTINCT CodigoProducto FROM productos
  WHERE Gama='Frutales'
  )c1 JOIN detallepedidos USING(CodigoProducto) JOIN pedidos USING(CodigoPedido) JOIN clientes USING(CodigoCliente) JOIN empleados ON clientes.CodigoEmpleadoRepVentas = empleados.CodigoEmpleado;


SELECT CodigoOficina FROM oficinas 
WHERE CodigoOficina NOT IN (
  SELECT DISTINCT CodigoOficina FROM (
  SELECT DISTINCT CodigoProducto FROM productos
  WHERE Gama='Frutales'
  )c1 JOIN detallepedidos USING(CodigoProducto) JOIN pedidos USING(CodigoPedido) JOIN clientes USING(CodigoCliente) JOIN empleados ON clientes.CodigoEmpleadoRepVentas = empleados.CodigoEmpleado
  );

-- (4.10.35) Código del frutal más vendido
SELECT CodigoProducto FROM productos
  WHERE Gama='Frutales';

SELECT c1.CodigoProducto, SUM(Cantidad)n FROM (
  SELECT CodigoProducto FROM productos
  WHERE Gama='Frutales'
  )c1 JOIN detallepedidos USING(CodigoProducto)
GROUP BY c1.CodigoProducto;

SELECT MAX(n) FROM (
  SELECT c1.CodigoProducto, SUM(Cantidad)n FROM (
    SELECT CodigoProducto FROM productos
    WHERE Gama='Frutales'
    )c1 JOIN detallepedidos USING(CodigoProducto)
    GROUP BY c1.CodigoProducto
  )c2;

SELECT c1.CodigoProducto, SUM(Cantidad)n FROM (
  SELECT CodigoProducto FROM productos
  WHERE Gama='Frutales'
  )c1 JOIN detallepedidos USING(CodigoProducto)
GROUP BY c1.CodigoProducto HAVING n=(
  SELECT MAX(n) FROM (
  SELECT c1.CodigoProducto, SUM(Cantidad)n FROM (
    SELECT CodigoProducto FROM productos
    WHERE Gama='Frutales'
    )c1 JOIN detallepedidos USING(CodigoProducto)
    GROUP BY c1.CodigoProducto
  )c2
  );


-- (4.10.37) Código del frutal con el que más se ha facturado

SELECT CodigoProducto FROM productos
  WHERE gama='Frutales';

SELECT CodigoProducto, SUM(Cantidad*PrecioUnidad)importe FROM 
  (SELECT CodigoProducto FROM productos WHERE gama='Frutales')c1 
  JOIN 
  detallepedidos
  USING(CodigoProducto)
  GROUP BY CodigoProducto;

SELECT MAX(importe) FROM (
  SELECT CodigoProducto, SUM(Cantidad*PrecioUnidad)importe FROM 
  (SELECT CodigoProducto FROM productos WHERE gama='Frutales')c1 
  JOIN 
  detallepedidos
  USING(CodigoProducto)
  GROUP BY CodigoProducto
  )c3;

SELECT CodigoProducto FROM 
  (SELECT CodigoProducto FROM productos WHERE gama='Frutales')c1 
  JOIN 
  detallepedidos
  USING(CodigoProducto)
  GROUP BY CodigoProducto HAVING SUM(Cantidad*PrecioUnidad) = (
  SELECT MAX(importe) FROM (
  SELECT CodigoProducto, SUM(Cantidad*PrecioUnidad)importe FROM 
  (SELECT CodigoProducto FROM productos WHERE gama='Frutales')c1 
  JOIN 
  detallepedidos
  USING(CodigoProducto)
  GROUP BY CodigoProducto
  )c3
  );