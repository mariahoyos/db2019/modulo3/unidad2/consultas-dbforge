﻿-- (4.10.38) Frutal con el que más se ha ganado (mayores beneficios)

SELECT CodigoProducto FROM productos
  WHERE Gama='Frutales';

SELECT c1.CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM (
  SELECT CodigoProducto FROM productos
  WHERE Gama='Frutales'
  )c1 JOIN productos USING(CodigoProducto);

SELECT CodigoProducto, SUM(beneficio*Cantidad)s FROM (
  SELECT c1.CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM (
  SELECT CodigoProducto FROM productos
  WHERE Gama='Frutales'
  )c1 JOIN productos USING(CodigoProducto)
  )c2 JOIN detallepedidos USING(CodigoProducto)
GROUP BY CodigoProducto;

SELECT MAX(s) FROM (
  SELECT CodigoProducto, SUM(beneficio*Cantidad)s FROM (
  SELECT c1.CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM (
  SELECT CodigoProducto FROM productos
  WHERE Gama='Frutales'
  )c1 JOIN productos USING(CodigoProducto)
  )c2 JOIN detallepedidos USING(CodigoProducto)
GROUP BY CodigoProducto
  )c3;

SELECT CodigoProducto FROM (
  SELECT c1.CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM (
  SELECT CodigoProducto FROM productos
  WHERE Gama='Frutales'
  )c1 JOIN productos USING(CodigoProducto)
  )c2 JOIN detallepedidos USING(CodigoProducto)
GROUP BY CodigoProducto HAVING SUM(beneficio*Cantidad)=(
  SELECT MAX(s) FROM (
  SELECT CodigoProducto, SUM(beneficio*Cantidad)s FROM (
  SELECT c1.CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM (
  SELECT CodigoProducto FROM productos
  WHERE Gama='Frutales'
  )c1 JOIN productos USING(CodigoProducto)
  )c2 JOIN detallepedidos USING(CodigoProducto)
GROUP BY CodigoProducto
  )c3
  );

-- (4.10.02b) Sacar el listado con los nombres de los clientes, el total pedido y el total pagado por cada uno de ellos.

SELECT NombreCliente, SUM(detallepedidos.Cantidad*PrecioUnidad), pagos.Cantidad FROM clientes JOIN pedidos USING(CodigoCliente) JOIN detallepedidos USING (CodigoPedido) JOIN pagos USING(CodigoCliente)
GROUP BY clientes.CodigoCliente
ORDER BY NombreCliente;

-- otra

-- c1
SELECT CodigoPedido, SUM(Cantidad*PrecioUnidad) s FROM detallepedidos
  GROUP BY CodigoPedido;

-- c2
SELECT CodigoCliente, SUM(s) pedidos FROM (
  SELECT CodigoPedido, SUM(Cantidad*PrecioUnidad) s FROM detallepedidos
  GROUP BY CodigoPedido
  )c1 JOIN pedidos USING(CodigoPedido)
  GROUP BY CodigoCliente;

-- c3
SELECT CodigoCliente, SUM(Cantidad) pagos FROM pagos
  GROUP BY CodigoCliente;

-- sol

SELECT NombreCliente, pedidos, pagos FROM clientes JOIN (
  SELECT CodigoCliente, SUM(s) pedidos FROM (
  SELECT CodigoPedido, SUM(Cantidad*PrecioUnidad) s FROM detallepedidos
  GROUP BY CodigoPedido
  )c1 JOIN pedidos USING(CodigoPedido)
  GROUP BY CodigoCliente
  )c2 USING(CodigoCliente)
  JOIN (
  SELECT CodigoCliente, SUM(Cantidad) pagos FROM pagos
  GROUP BY CodigoCliente
  )c3 USING(CodigoCliente)
ORDER BY NombreCliente;

-- (4.08.03b) Obtener los nombres de los clientes que hayan excedido su límite de crédito

SELECT * FROM pagos;

SELECT CodigoCliente, SUM(Cantidad) pago FROM pagos
  GROUP BY CodigoCliente;

SELECT NombreCliente FROM (
  SELECT CodigoCliente, SUM(Cantidad) pago FROM pagos
  GROUP BY CodigoCliente
  )c1 JOIN clientes USING(CodigoCliente)
  WHERE pago>LimiteCredito;

SELECT CodigoCliente, detallepedidos.CodigoPedido, CodigoProducto, Cantidad*PrecioUnidad cantidadProducto FROM pedidos JOIN detallepedidos USING(CodigoPedido);

-- c2

SELECT CodigoCliente, SUM(cantidadProducto)cantidadCliente FROM (
  SELECT CodigoCliente, detallepedidos.CodigoPedido, CodigoProducto, Cantidad*PrecioUnidad cantidadProducto FROM pedidos JOIN detallepedidos USING(CodigoPedido)
  )c1 GROUP BY CodigoCliente;

-- pagado

SELECT CodigoCliente, SUM(Cantidad) pagado FROM pagos
  GROUP BY CodigoCliente;

SELECT NombreCliente FROM (
    SELECT CodigoCliente, SUM(cantidadProducto)cantidadCliente FROM (
    SELECT CodigoCliente, detallepedidos.CodigoPedido, CodigoProducto, Cantidad*PrecioUnidad cantidadProducto FROM pedidos JOIN detallepedidos USING(CodigoPedido)
    )c1 GROUP BY CodigoCliente
  )c2 
  JOIN 
  (
  SELECT CodigoCliente, SUM(Cantidad) pagado FROM pagos
  GROUP BY CodigoCliente
  )pagado 
  ON c2.CodigoCliente=pagado.CodigoCliente
  JOIN clientes USING(CodigoCliente)
  WHERE cantidadCliente-pagado>LimiteCredito;