﻿-- (4.10.36) Código del frutal que más margen de beneficios proporciona
  SELECT CodigoProducto,PrecioVenta-PrecioProveedor beneficio FROM productos
    WHERE Gama= 'Frutales';

  SELECT MAX(beneficio) FROM (SELECT CodigoProducto,PrecioVenta-PrecioProveedor beneficio FROM productos
    WHERE Gama= 'Frutales')c1;

  SELECT CodigoProducto FROM (
    SELECT CodigoProducto,PrecioVenta-PrecioProveedor beneficio FROM productos
    WHERE Gama= 'Frutales'
    )c1 WHERE beneficio = (
     SELECT MAX(beneficio) FROM (SELECT CodigoProducto,PrecioVenta-PrecioProveedor beneficio FROM productos
     WHERE Gama= 'Frutales')c1
     );

-- (03) Obtener el nombre de los países desde donde no se han realizado pedidos.

SELECT DISTINCT Pais  FROM clientes;

SELECT distinct Pais FROM clientes JOIN pedidos USING(CodigoCliente);

SELECT Pais FROM (SELECT DISTINCT Pais  FROM clientes)c1 
WHERE Pais NOT IN (SELECT distinct Pais FROM clientes JOIN pedidos USING(CodigoCliente));

-- (4.09.04) Obtener el nombre de los clientes a los que no se les ha entregado a tiempo un pedido. Es decir, FechaEntrega mayor que FechaEsperada. Ordenar el resultado por orden alfabético

SELECT DISTINCT NombreCliente FROM pedidos JOIN clientes USING(CodigoCliente)
  WHERE FechaEntrega>FechaEsperada
  ORDER BY NombreCliente;

-- (4.10.03) Sacar el nombre de los clientes que hayan hecho pedidos en 2008

SELECT DISTINCT CodigoCliente FROM pedidos
  WHERE year(FechaPedido)=2008;

SELECT DISTINCT NombreCliente FROM (SELECT DISTINCT CodigoCliente FROM pedidos
  WHERE year(FechaPedido)=2008)c1 JOIN clientes USING(CodigoCliente);

-- (4.07.06b) Muestra el nombre de cliente repetido, ¿puede ser esto posible?

SELECT NombreCliente FROM clientes
  GROUP BY NombreCliente HAVING COUNT(*)>1;

-- (4.10.44) Con objeto de acortar distancias entre comerciales y sus clientes, obtener un listado de los códigos de los comerciales que están atendiendo clientes de fuera de su ciudad existiendo una oficina en la misma ciudad que el cliente. La idea es enviarles una circular y puedan realizar las gestiones oportunas para que sus clientes puedan ser atendidos por otro comercial más próximo geográficamente

-- c2

SELECT DISTINCT CodigoEmpleado comercial,Ciudad ce FROM empleados JOIN oficinas USING (CodigoOficina);

SELECT Ciudad cc, CodigoEmpleadoRepVentas FROM clientes
 WHERE Ciudad IN (SELECT DISTINCT CodigoEmpleado comercial,Ciudad FROM empleados JOIN oficinas USING (CodigoOficina));

-- c1

SELECT ciudad cc, CodigoEmpleadoRepVentas comercial FROM clientes;

SELECT * FROM (
  SELECT ciudad cc, CodigoEmpleadoRepVentas comercial FROM clientes
  )c1 JOIN 
  (
  SELECT DISTINCT CodigoEmpleado comercial,Ciudad ce FROM empleados JOIN oficinas USING (CodigoOficina)
  )c2
  USING (comercial) WHERE cc<>c3 AND cc IN(SELECT ce FROM c2);



-- otra



-- c1

SELECT DISTINCT Ciudad cc, CodigoEmpleadoRepVentas comercial FROM clientes;

-- c2

SELECT DISTINCT ciudad ce,CodigoEmpleado comercial FROM empleados JOIN oficinas USING(CodigoOficina);

-- sol

SELECT c1.comercial FROM (
  SELECT DISTINCT Ciudad cc, CodigoEmpleadoRepVentas comercial FROM clientes
  )c1 JOIN (
  SELECT DISTINCT ciudad ce,CodigoEmpleado comercial FROM empleados JOIN oficinas USING(CodigoOficina)
  )c2 ON c1.comercial = c2.comercial AND cc<>ce AND cc IN (SELECT DISTINCT ciudad ce FROM empleados JOIN oficinas USING(CodigoOficina));

-- (4.08.02) Obtener el nombre del producto del que más unidades se hayan vendido en un mismo pedido

SELECT MAX(Cantidad) FROM detallepedidos;

SELECT CodigoProducto FROM detallepedidos
  WHERE Cantidad = (SELECT MAX(Cantidad) FROM detallepedidos);

SELECT productos.Nombre FROM (SELECT CodigoProducto FROM detallepedidos
  WHERE Cantidad = (SELECT MAX(Cantidad) FROM detallepedidos))c1 JOIN productos USING(CodigoProducto);

-- (4.08.04) Sacar el códido de el/los producto/s que más unidades tiene en stock y el que menos

SELECT MAX(CantidadEnStock) FROM productos;

SELECT DISTINCT  CodigoProducto FROM productos
  WHERE CantidadEnStock = (SELECT MAX(CantidadEnStock) FROM productos) OR CantidadEnStock = (SELECT MIN(CantidadEnStock) FROM productos);

-- (4.10.44b) ¿A qué compañeros locales podrían pasarle el cliente aquellos comerciales que tienen que desplazarse para visitar a un cliente? Indica el código del comercial remoto en la primera columna, el código cada cliente en la segunda y el código de los posibles comerciales locales en la tercera, ordenado el resultado de forma que obtengamos una lista cómoda para que realicen la gestión los comerciales remotos

-- (4.10.13) Sacar el número de clientes que tiene asignado cada representante de ventas mostrando su nombre.

SELECT Nombre empleado, CodigoCliente FROM clientes JOIN empleados ON clientes.CodigoEmpleadoRepVentas = empleados.CodigoEmpleado;

SELECT empleado, COUNT(*) FROM (
  SELECT  DISTINCT Nombre empleado, CodigoCliente FROM clientes JOIN empleados ON clientes.CodigoEmpleadoRepVentas = empleados.CodigoEmpleado
  )c1 GROUP BY empleado;

-- otra

SELECT CodigoEmpleadoRepVentas, COUNT(*) FROM clientes
  GROUP BY CodigoEmpleadoRepVentas;

SELECT Nombre, n FROM (
  SELECT CodigoEmpleadoRepVentas, COUNT(*)n FROM clientes
  GROUP BY CodigoEmpleadoRepVentas
  )c1 JOIN empleados ON CodigoEmpleadoRepVentas = CodigoEmpleado;


-- (4.10.44b) ¿A qué compañeros locales podrían pasarle el cliente aquellos comerciales que tienen que desplazarse para visitar a un cliente? Indica el código del comercial remoto en la primera columna, el código de cada cliente en la segunda y el código de los posibles comerciales locales en la tercera, ordenado el resultado de forma que obtengamos una lista cómoda para que realicen la gestión los comerciales remotos

SELECT CodigoCliente, CodigoEmpleadoRepVentas, clientes.Ciudad cc, oficinas.Ciudad ce 
FROM clientes JOIN empleados ON clientes.CodigoEmpleadoRepVentas = empleados.CodigoEmpleado JOIN oficinas USING(CodigoOficina);

SELECT CodigoCliente, CodigoEmpleadoRepVentas, cc FROM (
  SELECT CodigoCliente, CodigoEmpleadoRepVentas, clientes.Ciudad cc, oficinas.Ciudad ce 
  FROM clientes JOIN empleados ON clientes.CodigoEmpleadoRepVentas = empleados.CodigoEmpleado JOIN oficinas USING(CodigoOficina)
  )c1 WHERE cc<>ce; 

SELECT DISTINCT  CodigoEmpleadoRepVentas, CodigoCliente, CodigoEmpleado FROM (
    SELECT CodigoCliente, CodigoEmpleadoRepVentas, cc FROM (
    SELECT CodigoCliente, CodigoEmpleadoRepVentas, clientes.Ciudad cc, oficinas.Ciudad ce 
    FROM clientes JOIN empleados ON clientes.CodigoEmpleadoRepVentas = empleados.CodigoEmpleado JOIN oficinas USING(CodigoOficina)
    )c1 WHERE cc<>ce
  )c2 JOIN oficinas ON cc=oficinas.Ciudad JOIN empleados USING(CodigoOficina)
ORDER BY 1, 2, 3;
