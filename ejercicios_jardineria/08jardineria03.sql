﻿-- (4.08.07) Facturación total del producto más caro

  SELECT MAX(PrecioVenta) FROM productos;

  SELECT CodigoProducto FROM productos
    WHERE PrecioVenta =  (SELECT MAX(PrecioVenta) FROM productos);

  SELECT SUM(Cantidad*PrecioUnidad) FROM (SELECT CodigoProducto FROM productos
    WHERE PrecioVenta =  (SELECT MAX(PrecioVenta) FROM productos))c1 JOIN detallepedidos USING(CodigoProducto);

-- (4.10.02) Sacar el listado con los nombres de los clientes y el total pagado por cada uno de ellos.

SELECT CodigoCliente, SUM(Cantidad) FROM pagos
  GROUP BY CodigoCliente;

SELECT NombreCliente, pagos FROM (SELECT CodigoCliente, SUM(Cantidad)pagos FROM pagos
  GROUP BY CodigoCliente)c1 JOIN clientes USING(CodigoCliente);

-- (01) Muestra el nombre y el código de cliente de aquellos clientes que han pedido alguno de los productos que nos aporta el mínimo beneficio con su venta. Ordena el resultado por nombre del cliente.

-- cálculo del beneficio de todos los productos
SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos;

-- cálculo del mínimo beneficio
SELECT MIN(beneficio) FROM (SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos)c1;

-- cálculo de los productos que aportan el mínimo beneficio
SELECT CodigoProducto FROM (
  SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos
  )c1 WHERE beneficio = (SELECT MIN(beneficio) FROM (SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos)c1);


-- cálculo de los pedidos que incluyen algún producto que proporciona mínimo beneficio
SELECT DISTINCT CodigoPedido FROM detallepedidos
  WHERE CodigoProducto IN (SELECT CodigoProducto FROM (
  SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos
  )c1 WHERE beneficio = (SELECT MIN(beneficio) FROM (SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos)c1));

-- cálculo de los clientes que han hecho algún pedido que incluye algún artículo que incluye algún producto que proporciona mínimo beneficio
SELECT DISTINCT NombreCliente,CodigoCliente FROM (SELECT DISTINCT CodigoPedido FROM detallepedidos
  WHERE CodigoProducto IN (SELECT CodigoProducto FROM (
  SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos
  )c1 WHERE beneficio = (SELECT MIN(beneficio) FROM (SELECT CodigoProducto, PrecioVenta-PrecioProveedor beneficio FROM productos)c1)))c2 JOIN pedidos USING(CodigoPedido) JOIN clientes USING(CodigoCliente)
ORDER BY 1;

-- (4.07.19) Sacar la facturación que ha tenido la empresa en toda la historia, indicando la base imponible, el IVA y el total facturado. NOTA: La base imponible se calcula sumando el coste del producto por el número de unidades vendidas. El IVA es el 21% de la base imponible y, el total, la suma de los dos campos anteriores.

-- calculo de la base imponible

SELECT SUM(PrecioProveedor*Cantidad)bi FROM
  productos JOIN detallepedidos USING(CodigoProducto)
  JOIN pedidos USING(CodigoPedido)
  WHERE  Estado<>'Rechazado' AND FechaEntrega IS NOT NULL;

-- solución

  SELECT DISTINCT Estado FROM pedidos;

SELECT ROUND(bi,2)BI,ROUND(0.21*bi,2)IVA,ROUND(1.21*bi,2) total FROM (
  SELECT SUM(PrecioProveedor*Cantidad)bi FROM 
  productos JOIN detallepedidos USING(CodigoProducto)
  JOIN pedidos USING(CodigoPedido)
  WHERE Estado<>'Rechazado' AND FechaEntrega IS NOT NULL  
  )c1;

-- (4.08.03) Obtener los nombres de los clientes cuya línea de crédito sea mayor que los pagos que haya realizado

-- cantidad pagada por cada cliente

SELECT CodigoCliente, SUM(Cantidad) pagos FROM pagos
GROUP BY CodigoCliente;

-- solucion

SELECT NombreCliente FROM (
  SELECT CodigoCliente, SUM(Cantidad) pagos FROM pagos GROUP BY CodigoCliente
  )c1 JOIN clientes USING(CodigoCliente) 
  WHERE LimiteCredito>c1.pagos;

-- (4.07.19) Sacar la facturación que ha tenido la empresa en toda la historia, indicando la base imponible, el IVA y el total facturado. NOTA: La base imponible se calcula sumando el coste del producto por el número de unidades vendidas. El IVA es el 21% de la base imponible y, el total, la suma de los dos campos anteriores.

-- Selección de los pedidos que vamos a tener en cuenta

  SELECT * FROM pedidos
    WHERE Estado<>'Rechazado' AND FechaEntrega IS NOT NULL;

-- base imponible

SELECT SUM(PrecioUnidad*Cantidad)bi FROM (
  SELECT * FROM pedidos
    WHERE Estado<>'Rechazado' AND FechaEntrega IS NOT NULL
  )c1 JOIN detallepedidos USING(CodigoPedido) JOIN productos USING(CodigoProducto);

-- solucion

SELECT ROUND(bi,2)BI,ROUND(0.21*bi,2)IVA,ROUND(1.21*bi,2) total FROM (
  SELECT SUM(PrecioProveedor*Cantidad)bi FROM 
  productos JOIN detallepedidos USING(CodigoProducto)
  JOIN pedidos USING(CodigoPedido)
  WHERE Estado<>'Rechazado' AND FechaEntrega IS NOT NULL  
  )c1;

SELECT ROUND(bi,2)BI,ROUND(0.21*bi,2)IVA,ROUND(1.21*bi,2) total FROM (
  SELECT SUM(PrecioUnidad*Cantidad)bi FROM (
  SELECT * FROM pedidos
    WHERE Estado<>'Rechazado' AND FechaEntrega IS NOT NULL
  )c1 JOIN detallepedidos USING(CodigoPedido) JOIN productos USING(CodigoProducto)
)c2;

-- (4.10.09) Sacar el nombre, apellidos, oficina (ciudad) y cargo del empleado que no represente a ningún cliente

SELECT DISTINCT CodigoEmpleadoRepVentas FROM clientes;

SELECT * FROM empleados
  WHERE CodigoEmpleado NOT IN (SELECT DISTINCT CodigoEmpleadoRepVentas FROM clientes);

SELECT Nombre, Apellido1, Apellido2, Ciudad, puesto FROM (
  SELECT * FROM empleados
  WHERE CodigoEmpleado NOT IN (SELECT DISTINCT CodigoEmpleadoRepVentas FROM clientes)
  )c1 JOIN oficinas USING(CodigoOficina);

-- (02) Mostrar el nombre de aquellos productos que se han pedido menos de 5 veces, ordenados alfabéticamente.

SELECT DISTINCT CodigoProducto FROM detallepedidos
  GROUP BY CodigoProducto HAVING COUNT(*)<5;

SELECT DISTINCT Nombre FROM (SELECT DISTINCT CodigoProducto FROM detallepedidos
  GROUP BY CodigoProducto HAVING COUNT(*)<5)c1 JOIN productos USING(CodigoProducto)
ORDER BY 1;

-- (4.10.18) Sacar los nombres de los clientes que han pedido más de 200 unidades de cualquier producto.

  -- esto está mal

SELECT CodigoPedido FROM detallepedidos
  WHERE Cantidad>200;

SELECT DISTINCT NombreCliente FROM (SELECT CodigoPedido FROM detallepedidos
  WHERE Cantidad>200)c1 JOIN pedidos USING (CodigoPedido) JOIN clientes USING(CodigoCliente);

-- bien

SELECT DISTINCT CodigoCliente FROM detallepedidos JOIN pedidos USING(CodigoPedido)
  GROUP BY CodigoCliente, CodigoProducto HAVING SUM(Cantidad)>200;

SELECT NombreCliente FROM (SELECT DISTINCT CodigoCliente FROM detallepedidos JOIN pedidos USING(CodigoPedido)
  GROUP BY CodigoCliente, CodigoProducto HAVING SUM(Cantidad)>200)c1 JOIN clientes USING(CodigoCliente);

-- (4.10.40) Códigos de los frutales no vendidos

SELECT DISTINCT CodigoProducto FROM productos
  WHERE Gama='Frutales' AND CodigoProducto NOT IN(SELECT DISTINCT c1.CodigoProducto FROM (SELECT DISTINCT CodigoProducto FROM productos
  WHERE Gama='Frutales')c1 JOIN detallepedidos USING(CodigoProducto));

SELECT DISTINCT c1.CodigoProducto FROM (SELECT DISTINCT CodigoProducto FROM productos
  WHERE Gama='Frutales')c1 JOIN detallepedidos USING(CodigoProducto);

-- (4.09.02) Sacar el nombre de los clientes que no hayan hecho pagos, junto al nombre de sus representantes (sin apellidos) y la ciudad de la oficina la que pertenece el representante. Ordena el resultado por el nombre del cliente.
-- por terminar

-- clientes que no han hecho pagos

SELECT CodigoCliente  FROM clientes
  WHERE CodigoCliente NOT IN (SELECT CodigoCliente FROM pagos);

-- clientes que han hecho pagos

SELECT CodigoCliente FROM pagos;

SELECT DISTINCT NombreCliente, Nombre, oficinas.Ciudad FROM 
  (SELECT CodigoCliente  FROM clientes
  WHERE CodigoCliente NOT IN (SELECT CodigoCliente FROM pagos))c1 
    JOIN clientes USING(CodigoCliente) 
    JOIN empleados ON clientes.CodigoEmpleadoRepVentas=CodigoEmpleado 
    JOIN oficinas USING(CodigoOficina)
  ORDER BY 1;

-- buensa

SELECT DISTINCT NombreCliente, Nombre, oficinas.Ciudad FROM 
  (SELECT CodigoCliente, CodigoEmpleadoRepVentas, NombreCliente  FROM clientes
  WHERE CodigoCliente NOT IN (SELECT CodigoCliente FROM pagos))c1 
    JOIN empleados ON c1.CodigoEmpleadoRepVentas=CodigoEmpleado 
    JOIN oficinas USING(CodigoOficina)
  ORDER BY 1;


