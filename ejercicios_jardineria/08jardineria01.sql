﻿-- (4.09.03) Obtener un listado con el nombre de los empleados junto con el nombre de sus jefes.

SELECT * FROM empleados;

SELECT Nombre empleado, CodigoJefe FROM empleados;

SELECT empleado, Nombre jefe FROM (SELECT Nombre empleado, CodigoJefe FROM empleados)c1 JOIN empleados ON CodigoEmpleado = c1.CodigoJefe;

-- (4.07.11) Clientes que no tiene asignado representante de ventas

SELECT * FROM clientes
  WHERE CodigoEmpleadoRepVentas IS NULL;

-- (4.07.14) Sacar los distintos estados por los que puede pasar un pedido

SELECT DISTINCT estado FROM pedidos;

-- (4.10.41) ¿De qué ciudades son los clientes?

SELECT DISTINCT ciudad FROM clientes;

-- (4.07.06) Sacar el nombre de los clientes españoles

SELECT * FROM clientes;

SELECT DISTINCT NombreCliente FROM clientes
  WHERE Pais = 'España' OR Pais = 'Spain';

-- (4.08.01) Obtener el nombre del producto más caro

SELECT MAX(PrecioVenta) FROM productos;

SELECT Nombre FROM productos
  WHERE PrecioVenta = (SELECT MAX(PrecioVenta) FROM productos);

-- (4.07.07) Sacar cuántos clientes tiene cada país

SELECT Pais, COUNT(*) FROM clientes
  GROUP BY Pais;

-- (4.07.10) Sacar el código de empleado y número de clientes al que atiende cada representante de ventas

SELECT CodigoEmpleado FROM empleados
  WHERE Puesto = 'Representante Ventas';

SELECT CodigoEmpleadoRepVentas, COUNT(*) FROM clientes
  GROUP BY CodigoEmpleadoRepVentas;
 
-- (4.10.11) Sacar los clientes que residan en la misma ciudad donde hay una oficina, indicando dónde está la oficina.

-- MySQL : Ciudad,NombreCliente,LineaDireccion1,LineaDireccion2 (La dirección sería la de la oficina)

SELECT DISTINCT ciudad,NombreCliente,oficinas.LineaDireccion1,oficinas.LineaDireccion2 FROM clientes JOIN oficinas USING(Ciudad);

-- (4.10.17) Sacar cuántos pedidos tiene cada cliente en cada estado. Mostrar el código del cliente, estado y número de pedidos

SELECT CodigoCliente,Estado,COUNT(*) FROM pedidos
  GROUP BY CodigoCliente, Estado;



