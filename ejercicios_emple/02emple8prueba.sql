﻿
USE emple_depart;
-- (10) Indicar el número de empleados más el número de departamentos

SELECT (SELECT COUNT(*) FROM emple)+(SELECT COUNT(*) FROM depart);


-- (20) Contar el número de empleados cuyo oficio sea VENDEDOR

SELECT DISTINCT COUNT(*) FROM emple
  WHERE oficio = 'VENDEDOR';

-- (23) Mostrar los apellidos del empleado que más gana

-- c1 salario máximo

  SELECT MAX(salario) FROM emple;

-- resultado 

  SELECT apellido FROM emple
    WHERE salario = (
        SELECT MAX(salario) FROM emple
      );

-- (29) Realizar un listado donde nos coloque el apellido del empleado y el nombre del departamento al que pertenece. Ordena el resultado por apellido.

SELECT emple.apellido, depart.dnombre FROM emple INNER JOIN depart ON depart.dept_no=emple.dept_no;

SELECT emple.apellido, depart.dnombre FROM emple JOIN depart USING (dept_no);

-- (30) Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado y el nombre del departamento al que pertenece. Ordenar los resultados por apellido de forma descendente.

SELECT emple.apellido, emple.oficio, depart.dnombre FROM
  emple JOIN depart USING (dept_no)
  ORDER BY apellido DESC;

-- (31) Listar el número de departamento de aquellos departamentos que tengan empleados y el número de empleados que tengan

-- departamentos con empleados

  SELECT DISTINCT dept_no FROM emple;

-- cuantos empleados hay en esos departamentos

SELECT dept_no, COUNT(*) FROM (SELECT DISTINCT dept_no FROM emple)c1 JOIN emple USING (dept_no); -- no funciona?

SELECT dept_no, COUNT(emp_no) FROM emple 
  GROUP BY dept_no;

-- (32) Listar el número de empleados por departamento, ordenados de más a menos empleados, incluyendo los departamentos que no tengan empleados. El encabezado de la tabla será dnombre y NUMERO_DE_EMPLEADOS

SELECT depart.dnombre, COUNT(emple.emp_no) FROM depart LEFT JOIN emple USING (dpto_no)
  GROUP BY depart.dept_no;

-- c1

SELECT dept_no, COUNT(*)n FROM emple
  GROUP BY dept_no;

-- resultado

SELECT dnombre, IFNULL(NUMERO_DE_EMPLEADOS,0) NUMERO_DE_EMPLEADOS FROM (
  SELECT dept_no, COUNT(*)NUMERO_DE_EMPLEADOS FROM emple
  GROUP BY dept_no
  )c1 RIGHT JOIN depart USING (dept_no)
ORDER BY NUMERO_DE_EMPLEADOS DESC;