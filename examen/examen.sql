﻿-- examen

-- (1) Provincia más poblada de las inferiores a un millón de habitantes

-- c1 - provincias menores a un millón de habitantes

SELECT * FROM provincias
  WHERE poblacion<1e6;

-- c2 - Población máxima de entre las que son inferiores a un millón de habitantes

SELECT MAX(poblacion) FROM (
  SELECT * FROM provincias
    WHERE poblacion<1e6
)c1;

-- solución

SELECT provincia FROM provincias
  WHERE poblacion = (SELECT MAX(poblacion) FROM (
  SELECT * FROM provincias
    WHERE poblacion<1e6
)c1);

-- (2) ¿En qué autonomía está la provincia más extensa?

-- (max de superficie)

SELECT MAX(superficie) FROM provincias;

-- c1 provincia de mayor superficie
  
SELECT provincia FROM provincias
  WHERE superficie = (SELECT MAX(superficie) FROM provincias);

-- agrupación por autonomias c2

SELECT autonomia,provincia FROM provincias
  GROUP BY autonomia;

-- autonomia en la que está la provincia c1

SELECT autonomia FROM provincias
  WHERE provincia = (SELECT provincia FROM provincias
  WHERE superficie = (SELECT MAX(superficie) FROM provincias));

-- (3) provincias con poblacion por encima de la media

SELECT AVG(poblacion) FROM provincias;

SELECT provincia FROM provincias
  WHERE poblacion>( 
    SELECT AVG(poblacion) FROM provincias
  );

-- (4) cuántas provincias tiene cada autonomía

SELECT autonomia, COUNT(provincia) FROM provincias
  GROUP BY autonomia;

-- (5) la anterior ordenando por más a menos provincias y por autonomia si coincide
SELECT autonomia, COUNT(provincia) FROM provincias
  GROUP BY autonomia
  ORDER BY 2 desc, autonomia;


SELECT COUNT(DISTINCT autonomia) FROM provincias;

SELECT DISTINCT autonomia FROM provincias
  WHERE autonomia NOT LIKE '% %';

EXPLAIN provincias;