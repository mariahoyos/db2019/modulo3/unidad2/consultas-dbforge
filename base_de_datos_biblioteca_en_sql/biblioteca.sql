﻿SET FOREIGN_KEY_CHECKS = 0;
DROP DATABASE IF EXISTS biblioteca;
CREATE DATABASE IF NOT EXISTS biblioteca;
USE biblioteca;

DROP TABLE IF EXISTS autor;
CREATE TABLE IF NOT EXISTS autor
(
numautor TINYINT(4) ZEROFILL AUTO_INCREMENT NOT NULL,
nombre VARCHAR(20) NOT NULL,
nacionalidad VARCHAR(20),
PRIMARY KEY (numautor), 
INDEX in_autor_1 (nombre)
)
ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_spanish_ci;

DROP TABLE IF EXISTS libro;
CREATE TABLE IF NOT EXISTS libro
(
numlibro TINYINT(4) ZEROFILL AUTO_INCREMENT NOT NULL,
titulo VARCHAR(30) NOT NULL,
año YEAR,
obras TEXT,
PRIMARY KEY (numlibro),
INDEX in_libro_1 (titulo)
)
ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_spanish_ci;

DROP TABLE IF EXISTS tema;
CREATE TABLE IF NOT EXISTS tema
(
tema VARCHAR(20) NOT NULL,
descripcion TEXT,
PRIMARY KEY (tema)
)
ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_spanish_ci;

DROP TABLE IF EXISTS obra;
CREATE TABLE IF NOT EXISTS obra
(
numobra TINYINT(4) ZEROFILL AUTO_INCREMENT NOT NULL,
titulo VARCHAR(30) NOT NULL,
año YEAR,
numautor TINYINT(4) ZEROFILL NOT NULL,
tema VARCHAR(20),
PRIMARY KEY (numobra),
INDEX in_obra_1 (titulo),
CONSTRAINT fk_obra_1 FOREIGN KEY (numautor) REFERENCES autor (numautor)
ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT fk_obra_2 FOREIGN KEY (tema) REFERENCES tema (tema)
ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_spanish_ci;

DROP TABLE IF EXISTS persona;
CREATE TABLE IF NOT EXISTS persona
(
numpersona TINYINT(4) ZEROFILL AUTO_INCREMENT NOT NULL,
nombre VARCHAR(30) NOT NULL,
telefono VARCHAR(15),
PRIMARY KEY (numpersona),
INDEX in_persona_1 (nombre)
)
ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_spanish_ci;

DROP TABLE IF EXISTS prestamo;
CREATE TABLE IF NOT EXISTS prestamo
(
numpersona TINYINT(4) ZEROFILL NOT NULL,
numlibro TINYINT(4) ZEROFILL NOT NULL,
fecha YEAR,
PRIMARY KEY (numpersona, numlibro),
CONSTRAINT fk_prestamo_1 FOREIGN KEY (numpersona) REFERENCES persona (numpersona)
ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT fk_prestamo_2 FOREIGN KEY (numlibro) REFERENCES libro (numlibro)
ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_spanish_ci;

DROP TABLE IF EXISTS esta_en;
CREATE TABLE IF NOT EXISTS esta_en
(
numobra TINYINT(4) ZEROFILL NOT NULL,
numlibro TINYINT(4) ZEROFILL NOT NULL,
PRIMARY KEY (numobra, numlibro),
CONSTRAINT fk_esta_en_1 FOREIGN KEY (numobra) REFERENCES obra (numobra)
ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT fk_esta_en_2 FOREIGN KEY (numlibro) REFERENCES libro (numlibro)
ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_spanish_ci;

INSERT INTO autor (nombre, nacionalidad) VALUES ('nom1', 'naci1');
INSERT INTO autor (nombre, nacionalidad) VALUES ('nom2', 'naci2');
INSERT INTO autor (nombre, nacionalidad) VALUES ('nom3', 'naci3');
INSERT INTO autor (nombre, nacionalidad) VALUES ('nom4', 'naci4');
INSERT INTO autor (nombre, nacionalidad) VALUES ('nom5', 'naci5');
INSERT INTO autor (nombre, nacionalidad) VALUES ('nom6', 'naci6');
INSERT INTO autor (nombre, nacionalidad) VALUES ('nom7', 'naci7');
INSERT INTO autor (nombre, nacionalidad) VALUES ('nom8', 'naci8');
INSERT INTO autor (nombre, nacionalidad) VALUES ('nom9', 'naci9');
INSERT INTO autor (nombre, nacionalidad) VALUES ('nom10', 'naci10');


INSERT INTO libro (titulo, año, obras) VALUES ('tit1', 1500, 'obr1');
INSERT INTO libro (titulo, año, obras) VALUES ('tit2', 1550, 'obr2');
INSERT INTO libro (titulo, año, obras) VALUES ('tit3', 1600, 'obr3');
INSERT INTO libro (titulo, año, obras) VALUES ('tit4', 1650, 'obr4');
INSERT INTO libro (titulo, año, obras) VALUES ('tit5', 1700, 'obr5');
INSERT INTO libro (titulo, año, obras) VALUES ('tit6', 1750, 'obr6');
INSERT INTO libro (titulo, año, obras) VALUES ('tit7', 1800, 'obr7');
INSERT INTO libro (titulo, año, obras) VALUES ('tit8', 1850, 'obr8');
INSERT INTO libro (titulo, año, obras) VALUES ('tit9', 1900, 'obr9');
INSERT INTO libro (titulo, año, obras) VALUES ('tit10', 1950, 'obr10');


INSERT INTO tema (tema, descripcion) VALUES ('tem1', 'descr1');
INSERT INTO tema (tema, descripcion) VALUES ('tem2', 'descr2');
INSERT INTO tema (tema, descripcion) VALUES ('tem3', 'descr3');
INSERT INTO tema (tema, descripcion) VALUES ('tem4', 'descr4');
INSERT INTO tema (tema, descripcion) VALUES ('tem5', 'descr5');


INSERT INTO obra (titulo, año, numautor, tema) VALUES ('oti1', 1900, 5, 'tem1');
INSERT INTO obra (titulo, año, numautor, tema) VALUES ('oti2', 1901, 6, 'tem2');
INSERT INTO obra (titulo, año, numautor, tema) VALUES ('oti3', 1902, 7, 'tem3');
INSERT INTO obra (titulo, año, numautor, tema) VALUES ('oti4', 1903, 8, 'tem4');
INSERT INTO obra (titulo, año, numautor, tema) VALUES ('oti5', 1904, 9, 'tem5');
INSERT INTO obra (titulo, año, numautor, tema) VALUES ('oti6', 1905, 10, 'tem1');
INSERT INTO obra (titulo, año, numautor, tema) VALUES ('oti7', 1906, 11, 'tem2');
INSERT INTO obra (titulo, año, numautor, tema) VALUES ('oti8', 1907, 12, 'tem3');
INSERT INTO obra (titulo, año, numautor, tema) VALUES ('oti9', 1908, 13, 'tem4');
INSERT INTO obra (titulo, año, numautor, tema) VALUES ('oti10', 1909, 14, 'tem5');


INSERT INTO persona (nombre, telefono) VALUES ('per1', 'tel1');
INSERT INTO persona (nombre, telefono) VALUES ('per2', 'tel2');
INSERT INTO persona (nombre, telefono) VALUES ('per3', 'tel3');
INSERT INTO persona (nombre, telefono) VALUES ('per4', 'tel4');
INSERT INTO persona (nombre, telefono) VALUES ('per5', 'tel5');
INSERT INTO persona (nombre, telefono) VALUES ('per6', 'tel6');
INSERT INTO persona (nombre, telefono) VALUES ('per7', 'tel7');
INSERT INTO persona (nombre, telefono) VALUES ('per8', 'tel8');
INSERT INTO persona (nombre, telefono) VALUES ('per9', 'tel9');
INSERT INTO persona (nombre, telefono) VALUES ('per10', 'tel10');


INSERT INTO prestamo (numpersona, numlibro, fecha) VALUES (10, 1, 2000);
INSERT INTO prestamo (numpersona, numlibro, fecha) VALUES (9, 2, 2001);
INSERT INTO prestamo (numpersona, numlibro, fecha) VALUES (8, 3, 2002);
INSERT INTO prestamo (numpersona, numlibro, fecha) VALUES (7, 4, 2003);
INSERT INTO prestamo (numpersona, numlibro, fecha) VALUES (6, 5, 2004);
INSERT INTO prestamo (numpersona, numlibro, fecha) VALUES (5, 6, 2005);
INSERT INTO prestamo (numpersona, numlibro, fecha) VALUES (4, 7, 2006);
INSERT INTO prestamo (numpersona, numlibro, fecha) VALUES (3, 8, 2007);
INSERT INTO prestamo (numpersona, numlibro, fecha) VALUES (2, 9, 2008);
INSERT INTO prestamo (numpersona, numlibro, fecha) VALUES (1, 10, 2010);


INSERT INTO esta_en (numobra, numlibro) VALUES (3,6);
INSERT INTO esta_en (numobra, numlibro) VALUES (4,7);
INSERT INTO esta_en (numobra, numlibro) VALUES (5,8);
INSERT INTO esta_en (numobra, numlibro) VALUES (6,9);
INSERT INTO esta_en (numobra, numlibro) VALUES (7,10);
INSERT INTO esta_en (numobra, numlibro) VALUES (8,1);
INSERT INTO esta_en (numobra, numlibro) VALUES (9,2);
INSERT INTO esta_en (numobra, numlibro) VALUES (10,3);
INSERT INTO esta_en (numobra, numlibro) VALUES (1,4);
INSERT INTO esta_en (numobra, numlibro) VALUES (2,5);

SET FOREIGN_KEY_CHECKS = 1;