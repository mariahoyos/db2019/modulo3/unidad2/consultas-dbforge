﻿-- (01) Lugares que nadie visita

SELECT * FROM lugares;

SELECT lugar FROM lugares LEFT JOIN visitan ON lugares.id_lugar = visitan.id_lugar WHERE visitan.id_lugar IS NULL;

-- (02) A qué destinos va cada persona

SELECT persona,lugar FROM personas JOIN visitan ON personas.id_persona = visitan.id_persona JOIN lugares ON visitan.id_lugar = lugares.id_lugar;

-- (03) Qué personas no viajan

SELECT persona FROM personas LEFT JOIN visitan ON personas.id_persona = visitan.id_persona WHERE visitan.id_persona IS null;

SELECT * FROM personas;

-- (04) Persona que más viaja

-- c1

SELECT id_persona, COUNT(*)n FROM visitan
  GROUP BY id_persona;

-- c2

SELECT MAX(n) FROM (
  SELECT id_persona, COUNT(*)n FROM visitan
    GROUP BY id_persona
)c1;

-- c3

SELECT id_persona FROM (
  SELECT id_persona, COUNT(*)n FROM visitan
  GROUP BY id_persona
  )c1 WHERE n=(
      SELECT MAX(n) FROM (
      SELECT id_persona, COUNT(*)n FROM visitan
        GROUP BY id_persona
    )c1
  );

-- sol

SELECT persona FROM (
    SELECT id_persona FROM (
    SELECT id_persona, COUNT(*)n FROM visitan
    GROUP BY id_persona
    )c1 WHERE n=(
        SELECT MAX(n) FROM (
        SELECT id_persona, COUNT(*)n FROM visitan
          GROUP BY id_persona
      )c1
    )
  )c3 JOIN personas USING (id_persona);

-- (05) destino al que han ido todas las personas

-- c1

SELECT id_lugar, COUNT(*) n FROM visitan
  GROUP BY id_lugar;

-- c2

SELECT MAX(n) FROM (
  SELECT id_lugar, COUNT(*) n FROM visitan
  GROUP BY id_lugar
  )c1;

-- c3

SELECT id_lugar FROM visitan
  GROUP BY id_lugar HAVING COUNT(*)=(SELECT MAX(n) FROM (
  SELECT id_lugar, COUNT(*) n FROM visitan
  GROUP BY id_lugar
  )c1);

-- sol

SELECT lugar FROM  (
    SELECT id_lugar FROM visitan
    GROUP BY id_lugar HAVING COUNT(*)=(SELECT MAX(n) FROM (
    SELECT id_lugar, COUNT(*) n FROM visitan
    GROUP BY id_lugar
    )c1)
  )c3 JOIN lugares USING (id_lugar);

-- (06) Destinos al que viajarán todas las personas que viajan

SELECT COUNT(DISTINCT id_persona) FROM visitan;

SELECT id_lugar FROM visitan
  GROUP BY id_lugar HAVING COUNT(*)=(
    SELECT COUNT(DISTINCT id_persona) FROM visitan
);

SELECT lugar FROM (
      SELECT id_lugar FROM visitan
      GROUP BY id_lugar HAVING COUNT(*)=(
        SELECT COUNT(DISTINCT id_persona) FROM visitan
    )
  )c1 JOIN lugares USING(id_lugar);




-- (07) Personas que viajarán a todos los destinos que se visitarán

-- c1

SELECT COUNT(DISTINCT id_lugar) FROM visitan;

-- c2

SELECT id_persona FROM visitan
  GROUP BY id_persona HAVING COUNT(*) = (SELECT COUNT(DISTINCT id_lugar) FROM visitan);

-- solución

SELECT persona FROM ( 
  SELECT id_persona FROM visitan
  GROUP BY id_persona HAVING COUNT(*) = (SELECT COUNT(DISTINCT id_lugar) FROM visitan)
  )c3 JOIN personas USING (id_persona);

-- (08) personas que sólo piensan hacer un viaje

-- c1: personas que sólo han hecho un viaje
SELECT id_persona FROM visitan
  GROUP BY id_persona HAVING COUNT(*)=1;



SELECT persona FROM (
  SELECT id_persona FROM visitan
  GROUP BY id_persona HAVING COUNT(*)=1
  )c1 JOIN personas ON c1.id_persona=personas.id_persona;

