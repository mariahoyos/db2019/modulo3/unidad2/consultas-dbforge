﻿SHOW TABLES;

-- Creación de tablas

CREATE OR REPLACE TABLE personas(
  id_persona int AUTO_INCREMENT,
  persona varchar(31),
  PRIMARY KEY(id_persona),
  UNIQUE(persona)
);

CREATE OR REPLACE TABLE lugares(
  id_lugar int AUTO_INCREMENT,
  lugar varchar(31),
  PRIMARY KEY(id_lugar),
  UNIQUE(lugar)
);

CREATE OR REPLACE TABLE visitan(
  id_viaje int AUTO_INCREMENT,
  id_persona int,
  id_lugar int,
  fecha date,
  PRIMARY KEY(id_viaje)
);

-- Constrainsts

ALTER TABLE visitan
  ADD CONSTRAINT fk_visitan_persona
   FOREIGN KEY (id_persona)
   REFERENCES personas(id_persona),

  ADD CONSTRAINT fk_visitan_lugares
  FOREIGN KEY (id_lugar)
  REFERENCES lugares(id_lugar);

-- Inserciones

INSERT INTO personas (persona) VALUES 
('Isabel'),('Antonio'),('Jhonny'),('Roberto'),('María');

INSERT INTO lugares(lugar) VALUES
  ('China'),('Hawai'),('Bogotá'),('Madrid'),('New York');

INSERT INTO visitan(id_persona,id_lugar) VALUES
  (1,1),(2,2),(3,3),(4,4),(5,5);

INSERT INTO lugares(lugar) VALUES ('Noja');

INSERT INTO visitan(id_lugar, id_persona) VALUES
  (2,4),(4,5),(5,3);

INSERT INTO visitan (id_persona, id_lugar)
  VALUES (4,5);

INSERT INTO visitan (id_persona, id_lugar)
  VALUES (4,3);

ALTER TABLE visitan
  ADD CONSTRAINT uk_visitan_id_persona_id_lugar UNIQUE KEY (id_persona,id_lugar);

-- DELETE: a la hora de hacer un delete hay que hacer un select primero CON WHERE y luego se cambiar select * por DELETE

SELECT id_persona FROM personas WHERE persona='Isabel';

DELETE FROM visitan WHERE id_persona=(SELECT id_persona FROM personas WHERE persona='Isabel');


